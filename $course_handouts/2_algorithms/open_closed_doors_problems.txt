There are one thousand lockers (all CLOSED initially) and there are one thousand students in a 
school. A teacher asks the students to do the following:

The first student must go to every locker and open it. 
Then he has the second student go to every second locker and closes it. 
The third student goes to every third locker and, if it is closed, he opens it, and 
if it is open, he closes it. 
The fourth student goes to every fourth locker and, if it is closed, he opens it, 
and if it is open, he closes it, 
and so on. After the process is completed with the thousandth student, 
how many lockers are open?

