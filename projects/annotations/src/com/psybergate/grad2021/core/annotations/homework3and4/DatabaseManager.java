package com.psybergate.grad2021.core.annotations.homework3and4;

import com.psybergate.grad2021.core.annotations.homework1and2.Customer;
import com.psybergate.grad2021.core.annotations.homework1and2.DomainProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.time.LocalDate;

public class DatabaseManager {

  public static final String USERNAME = "postgres";

  public static final String PASSWORD = "admin";

  public static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/postgres";

  public static void main(String[] args) {
    DatabaseManager databaseManager = new DatabaseManager();
    System.out
        .println("databaseManager.tableExists(databaseManager.getDatabaseConnection(), \"'customer'\") = " + databaseManager
            .tableExists(databaseManager.getDatabaseConnection(), "'customer'"));
  }

  private Connection getDatabaseConnection() {
    Connection connection = null;
    try {
      Class.forName("org.postgresql.Driver");
      connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Opened database successfully");
    return connection;
  }

  public boolean tableExists(Connection connection, String tableName) {

    try {
      ResultSet resultSet = connection.getMetaData().getTables(null, null, tableName, null);

      while(resultSet.next()){
        System.out.println("here");
        return true;
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return false;

  }

  private String getTableAttributes(Field[] fields) {
    String tableAttributes = "";

    for (Field field : fields) {
      if (fieldIsDomainProperty(field)) {
        tableAttributes += (field.getName() + " " + getFieldType(field) + ",");
      }
    }
    return tableAttributes.substring(0, tableAttributes.length() - 1);
  }

  private String getFieldType(Field field) {
    switch (field.getType().getSimpleName()) {
      case ("String"):
        return "varchar(250)";
      case ("Integer"):
        return "int";
      case ("LocalDate"):
        return "date";
      default:
        return "";
    }
  }

  private boolean fieldIsDomainProperty(Field field) {
    Annotation[] annotations = field.getDeclaredAnnotations();
    for (Annotation annotation : annotations) {
      if (annotation instanceof DomainProperty) {
        return true;
      }
    }
    return false;
  }

  public void createTable(Connection connection, String tableName, Field[] fields) {

    if (tableExists(connection, tableName)) {
      System.out.println("Table already Exists");
    } else {
      String query = "create table " + tableName + " (" + getTableAttributes(fields) + ");";
//    System.out.println("query = " + query);
      Statement statement = null;
      try {

        statement = connection.createStatement();
        statement.executeUpdate(query);
        System.out.println("Query successfully executed");
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }
    }
  }

  public void insertIntoDatabase(Customer customer){

    Connection connection = getDatabaseConnection();

    String query = "insert into " + customer.getClass().getSimpleName() + getAttributes(customer) + " values " + getValues(customer) + ";";

    System.out.println("query = " + query);

    Statement statement = null;

    try {
      statement = connection.createStatement();
      statement.executeUpdate(query);
      System.out.println("Customer inserted successfully");
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  private String getValues(Customer customer) {
    String values = "(" + "'" +customer.getCustomerNum() + "'" + "," + "'" + customer.getCustomerName() + "'" + "," + "'" + customer.getCustomerSurname() + "'" + "," + "'" + customer.getDateOfBirth() + "'" + ")";
    return values;
  }

  private String getAttributes(Customer customer) {
    String attributes = " (";

    for (Field field : customer.getClass().getDeclaredFields()) {
      if(fieldIsDomainProperty(field)){
        attributes += field.getName() + ",";
      }
    }

    return attributes.substring(0, attributes.length()-1) + ")" ;
  }

  public void generateDatabaseTable(Class clazz) {
    DatabaseManager databaseManager = new DatabaseManager();
    databaseManager.createTable(getDatabaseConnection(), clazz.getSimpleName(), clazz.getDeclaredFields());
  }

  public Customer getSpecificCustomerData(String customerNum) {

    Connection connection = getDatabaseConnection();
    String query = "select * from customer where customernum = '" + customerNum + "';";
//    System.out.println("query = " + query);
    Customer customer = null;
    Statement statement = null;

    try {
      statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(query);
      System.out.println("Customer retrieved successfully");
      while (resultSet.next()) {
        customer = new Customer(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3),
            calculateLocalDate(resultSet.getString(4)));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return customer;
  }

  private LocalDate calculateLocalDate(String string) {
    String[] localDate = string.split("-");
    return LocalDate.of(Integer.parseInt(localDate[0]), Integer.parseInt(localDate[1]), Integer.parseInt(localDate[2]));
  }
}
