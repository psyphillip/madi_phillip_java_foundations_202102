package com.psybergate.grad2021.core.annotations;

import com.psybergate.grad2021.core.annotations.homework1and2.Customer;
import com.psybergate.grad2021.core.annotations.homework3and4.DatabaseManager;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.time.LocalDate;

public class TestingRandomStuff {
  public static void main(String[] args) {
    Customer customer = new Customer("123", "Phill", "Madi", LocalDate.of(1990,4,8));

    for (Field field : customer.getClass().getDeclaredFields()) {
      System.out.println("field.getName() = " + field.getName());
    }

    dateManipulation();

//    databaseStuff();
  }

  private static void dateManipulation(){
    LocalDate localDate = LocalDate.now();
    System.out.println(localDate.toString());
  }

  private static void databaseStuff() {
    DatabaseManager databaseManager = new DatabaseManager();

//    Connection connection = databaseManager.getDatabaseConnection();

//    System.out.println("databaseManager.tableExists(connection, \"Customer\") = " + databaseManager.tableExists(connection, "Customer"));
    Class customerClass = Customer.class;

//    System.out.println("customerClass.getName() = " + customerClass.getSimpleName());
//
//    Annotation[] customerClassAnnotations = customerClass.getAnnotations();
    Field[] customerFields = customerClass.getDeclaredFields();
//    System.out.println("customerFields.length = " + customerFields.length);
//    for (Field customerField : customerFields) {
//      System.out.println("customerField = " + customerField.getType().getSimpleName());
//      for (Annotation fieldAnnotations : customerField.getDeclaredAnnotations()) {
//        System.out.println("fieldAnnotations.annotationType().getName() = " + fieldAnnotations.annotationType().getSimpleName());
//      }
//    }

//    System.out.println("customerClassAnnotations.length = " + customerClassAnnotations.length);
//
//    for (Annotation customerClassAnnotation : customerClassAnnotations) {
//      System.out.println("customerClassAnnotation.toString() = " + customerClassAnnotation.toString());
//    }

//    databaseManager.createTable(connection, customerClass.getSimpleName(), customerFields);
  }
}
