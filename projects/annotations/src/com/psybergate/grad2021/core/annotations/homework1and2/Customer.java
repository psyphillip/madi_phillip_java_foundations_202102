package com.psybergate.grad2021.core.annotations.homework1and2;

import java.time.LocalDate;
import java.time.Period;

@DomainClass
public class Customer {

  @DomainProperty
  private String customerNum;

  @DomainProperty
  private String CustomerName;

  @DomainProperty
  private String CustomerSurname;

  @DomainProperty
  private LocalDate dateOfBirth;

  @DomainTransient
  private int age;

  public Customer(String customerNum, String CustomerName, String CustomerSurname, LocalDate dateOfBirth) {
    this.customerNum = customerNum;
    this.CustomerName = CustomerName;
    this.CustomerSurname = CustomerSurname;
    this.dateOfBirth = dateOfBirth;
    this.age = calculateAge(LocalDate.now(), dateOfBirth);
  }

  private int calculateAge(LocalDate now, LocalDate dateOfBirth) {
    Period difference = Period.between(dateOfBirth, now);
    return difference.getYears();
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getCustomerName() {
    return CustomerName;
  }

  public String getCustomerSurname() {
    return CustomerSurname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public int getAge() {
    return age;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "customerNum='" + customerNum + '\'' +
        ", CustomerName='" + CustomerName + '\'' +
        ", CustomerSurname='" + CustomerSurname + '\'' +
        ", dateOfBirth=" + dateOfBirth +
        ", age=" + age +
        '}';
  }
}
