package com.psybergate.grad2021.core.annotations.homework3and4;

import com.psybergate.grad2021.core.annotations.homework1and2.Customer;

import java.time.LocalDate;

public class CustomerService {

  public static void main(String[] args) {
    //saveCustomer();
    System.out.println("getCustomer(\"123\") = " + getCustomer("123"));
  }

  public static void saveCustomer(){
    Customer customer = new Customer("123", "Phil", "Madi", LocalDate.of(1990, 4, 3));
    DatabaseManager databaseManager = new DatabaseManager();
    databaseManager.insertIntoDatabase(customer);
  }

  public static Customer getCustomer(String customerNum){
    DatabaseManager databaseManager = new DatabaseManager();
    Customer customer = databaseManager.getSpecificCustomerData(customerNum);
    return customer;
  }
}
