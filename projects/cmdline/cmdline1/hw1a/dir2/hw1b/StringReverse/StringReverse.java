package hw1b.StringReverse;

import hw1a.MyStringUtils.*;

public class StringReverse{

	public static void reverse(){
		System.out.println(StringUtils.reverse("Phillip"));
	}

	public static void main(String[] args) {
		reverse();
	}
}