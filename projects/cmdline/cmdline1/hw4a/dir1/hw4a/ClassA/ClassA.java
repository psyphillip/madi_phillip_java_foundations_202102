package hw4a.ClassA;

import hw4b.ClassB.*;

public class ClassA{

	public static void callClassA(){
		System.out.println("Called Class A");
	}

	public static void main(String[] args) {
		ClassB.callClassB();
	}
}