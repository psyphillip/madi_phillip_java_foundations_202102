package com.psybergate.grad2021.core.oopart2.hw5a2_2;

import java.time.LocalDate;
import java.time.Period;

public class Customer {

  private String customerNum;

  private String customerName;

  private String customerType;

  private String customerStartDate;

  public Customer(String customerNum, String customerName, String customerType, String customerStartDate) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerType = customerType;
    this.customerStartDate = customerStartDate;
  }

  public int getCustomerDuration() {
    LocalDate customerDate =
        LocalDate.of(getCustomerStartDateYear(), getCustomerStartDateMonth(), getCustomerStartDateDay());
    return getCustomerYears(customerDate);
  }

  private int getCustomerYears(LocalDate customerDate) {
    return Period.between(customerDate, LocalDate.now()).getYears();
  }

  public String getCustomerNum() {
    return customerNum;
  }

  private String getCustomerStartDate() {
    return customerStartDate;
  }

  private int getCustomerStartDateYear() {
    return Integer.parseInt(getCustomerStartDate().split("/")[0]);
  }

  private int getCustomerStartDateMonth() {
    return Integer.parseInt(getCustomerStartDate().split("/")[1]);
  }

  private int getCustomerStartDateDay() {
    return Integer.parseInt(getCustomerStartDate().split("/")[2]);
  }
}
