package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String name;

  private String customerNum;

  private String customerType;

  private int customerDuration; //start date

  private List<Order> orders = new ArrayList<Order>(); // Better to model this in order

  public Customer(String name, String customerNum, String customerType, int customerDuration) {
    this.name = name;
    this.customerNum = customerNum;
    this.customerType = customerType;
    this.customerDuration = customerDuration;
  }

  public double getTotalValue() {
    double totalValue = 0;

    for (Order order : orders) {
      totalValue += order.getTotalValue(this);
    }

    return totalValue;
  }

  public void addOrderItem(Order order, String name, double price, int quantity) {
    order.addOrderItem(name, price, quantity);
  }

  public void makeNewOrder() {
    LocalDate date = LocalDate.now();

    if (customerType.equals("LocalCustomer")) {
      orders.add(new LocalOrder("123", date, 0.1));
    } else {
      orders.add(new InternationalOrder("123", date, 1000));
    }
  }

  public int getCustomerDuration() {
    return customerDuration;
  }

  public List<Order> getOrders() {
    return orders;
  }

  public String getCustomerNum() {
    return customerNum;
  }
}
