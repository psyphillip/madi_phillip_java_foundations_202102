package com.psybergate.grad2021.core.oopart2.hw2a;

public class RectangleUtils {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(10,20);
        Rectangle rectangle2 = new Rectangle(10,20);
        Rectangle rectangle3 = rectangle1;

        /**
         * rectangle1 is identical to rectangle3 because they are pointing to the same object
         * rectangle1 is not identical to rectangle 2 because they are pointing to two different objects
         */

        System.out.println("rectangle1 == rectangle3 = " + (rectangle1 == rectangle3));
        System.out.println("rectangle1 == rectangle2 = " + (rectangle1 == rectangle2));

        /**
         * rectangle1 is equal to rectangle2 because they point to objects with equal states
         */
        System.out.println("rectangle1.equals(rectangle2) = " + rectangle1.equals(rectangle2));
        System.out.println("rectangle3.equals(rectangle2) = " + rectangle3.equals(rectangle2));
    }
}
