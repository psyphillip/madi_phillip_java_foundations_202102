package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class Account {

    protected String accountNum;
    protected String balance;

    public Account(String accountNum, String balance) {
        this.accountNum = accountNum;
        this.balance = balance;
        System.out.println("Parent constructor done!");
    }
}