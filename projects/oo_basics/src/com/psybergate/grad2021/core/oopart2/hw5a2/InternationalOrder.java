package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;

public class InternationalOrder extends Order {

    public static final String INTERNATIONAL_ORDER = "InternationalOder";

    public InternationalOrder(String orderNum, LocalDate date, Customer customer) {
        super(orderNum, date, customer);
    }

    public double getTotalValue() {
        double totalValue = super.getTotalValue();

        if(totalValue > 500_000 && totalValue <= 1_000_000){
            return totalValue - (totalValue*0.05);
        }

        else if(totalValue > 1_000_000){
            return totalValue - (totalValue*0.1);
        }

        return totalValue;
    }

    public String getOrderType() {
        return INTERNATIONAL_ORDER;
    }
}
