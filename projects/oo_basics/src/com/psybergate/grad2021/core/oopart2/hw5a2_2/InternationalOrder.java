package com.psybergate.grad2021.core.oopart2.hw5a2_2;

public class InternationalOrder extends Order {
  public static final String INTERNATIONAL_ODER = "InternationalOder";

  private static double lowDiscountValue;

  private static double highDiscountValue;

  private static double lowDiscountRate;

  private static double highDiscountRate;

  static {
    lowDiscountValue = 500_000;
    highDiscountValue = 1_000_000;
    highDiscountRate = 0.1;
    lowDiscountRate = 0.05;
  }

  public InternationalOrder(String orderNum, Customer customer) {
    super(orderNum, customer);
  }

  public double getTotalOrderCost() {
    double totalOrderCost = super.getTotalOrderCost();

    if (totalOrderCost > lowDiscountValue && totalOrderCost <= highDiscountValue) {
      return totalOrderCost - (totalOrderCost * lowDiscountRate);
    } else if (totalOrderCost > highDiscountValue) {
      return totalOrderCost - (totalOrderCost * highDiscountRate);
    }
    return totalOrderCost;
  }

  public static String getInternationalOder() {
    return INTERNATIONAL_ODER;
  }
}
