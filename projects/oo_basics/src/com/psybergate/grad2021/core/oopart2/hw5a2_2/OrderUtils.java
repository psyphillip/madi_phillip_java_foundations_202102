package com.psybergate.grad2021.core.oopart2.hw5a2_2;

import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.ArrayList;
import java.util.List;

/**
 * Please keep date format in the following order "YYYY/MM/DD"
 */

public class OrderUtils {

  public static void main(String[] args) {
    List<Customer> customers = new ArrayList<Customer>();
    List<Order> orders = new ArrayList<Order>();

    Customer phillip = new Customer(String.valueOf(customers.size()), "Phillip", "Local Customer", "2009/09/16");
    customers.add(phillip);

    Order firstOrderOfPhillip = new LocalOrder(String.valueOf(orders.size()), phillip);
    firstOrderOfPhillip.addOrderItem("Rubber", 2);
    orders.add(firstOrderOfPhillip);

    Order secondOrderOfPhillip = new LocalOrder(String.valueOf(orders.size()), phillip);
    secondOrderOfPhillip.addOrderItem("Rubber", 2);
    orders.add(secondOrderOfPhillip);

    Order thirdOrderOfPhillip = new LocalOrder(String.valueOf(orders.size()), phillip);
    thirdOrderOfPhillip.addOrderItem("Phone", 3);

    orders.add(thirdOrderOfPhillip);

    Order forthOrderOfPhillip = new LocalOrder(String.valueOf(orders.size()), phillip);
    orders.add(forthOrderOfPhillip);

    calculateTotalOrderValue(customers, orders);

  }

  public static void calculateTotalOrderValue(List<Customer> customers, List<Order> orders) {
    for (Customer customer : customers) {

      System.out.println("Total Orders for Customer " + customer.getCustomerNum());

      double totalOrderValue = 0;

      for (Order order : orders) {
        if (order.getCustomer().equals(customer)) {
          System.out.println("Order number " + order.getOrderNum());
          System.out.println("Total Order Cost :R" + order.getTotalOrderCost());
          totalOrderValue += order.getTotalOrderCost();
        }
      }
      System.out.println("totalOrderValue = " + totalOrderValue);
    }
  }
}
