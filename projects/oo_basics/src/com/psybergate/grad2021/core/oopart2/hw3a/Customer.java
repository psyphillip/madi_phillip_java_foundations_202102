package com.psybergate.grad2021.core.oopart2.hw3a;

import java.util.Objects;

public class Customer {
    private String customerNum;
    private String customerName;

    public Customer(String customerNum, String customerName) {
        this.customerNum = customerNum;
        this.customerName = customerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(customerNum, customer.customerNum);
    }

}
