package com.psybergate.grad2021.core.oopart2.hw5a2_2;

public class LocalOrder extends Order {
  private static final String LOCAL_ORDER = "Local Order";

  private static int lowCustomerDuration;

  private static int highCustomerDuration;

  private static double highDiscountRate;

  private static double lowDiscountRate;

  static {
    lowCustomerDuration = 2;
    highCustomerDuration = 5;
    lowDiscountRate = 0.075;
    highDiscountRate = 0.125;
  }

  public LocalOrder(String orderNum, Customer customer) {
    super(orderNum, customer);
  }

  @Override
  public double getTotalOrderCost() {
    double totalOrderCost = super.getTotalOrderCost();
    if (customer.getCustomerDuration() > lowCustomerDuration && customer
        .getCustomerDuration() <= highCustomerDuration) {
      return totalOrderCost - (totalOrderCost * lowDiscountRate);
    } else if (customer.getCustomerDuration() > highCustomerDuration) {
      return totalOrderCost - (totalOrderCost * highDiscountRate);
    }
    return totalOrderCost;
  }

  public static String getLocalOrder() {
    return LOCAL_ORDER;
  }
}
