package com.psybergate.grad2021.core.oopart2.hw6a;

import java.util.ArrayList;
import java.util.List;

public class LocalRates {

    List<Integer> customerDuration = new ArrayList<Integer>();
    List<Double> discountRates = new ArrayList<Double>();

    public List<Integer> getCustomerDuration() {
        return customerDuration;
    }

    public List<Double> getDiscountRates() {
        return discountRates;
    }

    public String addCustomerDuration(int year){
        if (customerDuration.contains(year)) {
            return "year already exists";
        }
        customerDuration.add(year);
        return "Successful";
    }

    public String addDiscountRate(double rate){
        if (discountRates.contains(rate)) {
            return "rate already exist";
        }
        discountRates.add(rate);
        return "successful";
    }
}
