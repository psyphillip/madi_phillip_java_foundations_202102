package com.psybergate.grad2021.core.oopart2.demo01;

public class ShapeUtils {
    public static void main(String[] args) {
        Shape shape = new Square(2,2);

        shape.printSide1(); //Executes the method in Shape and not the one in Square (NOT POLYMORPHIC)

        shape.getNameOfShape(); //Executes the method in Square and not the one in square (POLYMORPHIC)
    }
}
