package com.psybergate.grad2021.core.oopart2.hw5a2_2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

  private String orderNum;

  protected Customer customer;

  private LocalDate orderDate = LocalDate.now();

  private List<OrderItem> orderItems = new ArrayList<OrderItem>();

  public Order(String orderNum, Customer customer) {
    this.orderNum = orderNum;
    this.customer = customer;
  }

  public void addOrderItem(String productName, int quantity) {
    orderItems.add(new OrderItem(productName, quantity));
  }

  public Customer getCustomer() {
    return customer;
  }

  public double getTotalOrderCost() {
    double totalCost = 0;

    for (OrderItem orderItem : orderItems) {
      totalCost += orderItem.getItemTotal();
    }
    return totalCost;
  }

  public String getOrderNum() {
    return orderNum;
  }

  public LocalDate getOrderDate() {
    return orderDate;
  }
}
