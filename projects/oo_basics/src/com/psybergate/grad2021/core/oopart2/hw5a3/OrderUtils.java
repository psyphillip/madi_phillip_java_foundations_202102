package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
    public static void main(String[] args) {

        // First Customer
        Customer customer1 = new Customer("phillip", "5", "InternationalCustomer", 1);

        //First Order
        customer1.makeNewOrder("InternationalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(0), "Trouser", 600, 5);
        customer1.addOrderItem(customer1.getOrders().get(0), "T-Shirt", 120, 3);
        customer1.addOrderItem(customer1.getOrders().get(0), "Socks", 50, 10);

        //Second Order
        customer1.makeNewOrder("LocalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(1), "Doritos", 19, 4);
        customer1.addOrderItem(customer1.getOrders().get(1), "Romany", 22, 2);
        customer1.addOrderItem(customer1.getOrders().get(1), "Maynards", 10, 3);
        customer1.addOrderItem(customer1.getOrders().get(1), "Cheppies", 1, 10);

        //Third Order
        customer1.makeNewOrder("InternationalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(2), "Mitsubishi", 300_000, 1);
        customer1.addOrderItem(customer1.getOrders().get(2), "Hyundai", 250_000, 1);

        //Forth Order
        customer1.makeNewOrder("InternationalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(3), "Farm", 5_000_000, 1);


        // Second Customer
        Customer customer2 = new Customer("Karabo", "4", "LocalCustomer", 6);

        //First Order
        customer2.makeNewOrder("InternationalCustomer");
        customer2.addOrderItem(customer2.getOrders().get(0), "Trouser", 500, 7);
        customer2.addOrderItem(customer2.getOrders().get(0), "T-Shirt", 150, 5);
        customer2.addOrderItem(customer2.getOrders().get(0), "Socks", 80, 7);

        //Second Order
        customer2.makeNewOrder("InternationalCustomer");
        customer2.addOrderItem(customer2.getOrders().get(1), "Apartment", 550_000, 1);
        customer2.addOrderItem(customer2.getOrders().get(1), "TownHouse", 600_000, 1);
        customer2.addOrderItem(customer2.getOrders().get(1), "House", 750_000, 1);
        customer2.addOrderItem(customer2.getOrders().get(1), "Farm", 3_500_000, 1);

        //Third Order
        customer2.makeNewOrder("LocalCustomer");
        customer2.addOrderItem(customer2.getOrders().get(2), "Toyota", 300_000, 1);
        customer2.addOrderItem(customer2.getOrders().get(2), "VW", 450_000, 1);


        // Third Customer
        Customer customer3 = new Customer("Hope", "7", "LocalCustomer", 3);

        //First Order
        customer3.makeNewOrder("LocalCustomer");
        customer3.addOrderItem(customer3.getOrders().get(0), "Trouser", 750, 3);
        customer3.addOrderItem(customer3.getOrders().get(0), "T-Shirt", 120, 1);
        customer3.addOrderItem(customer3.getOrders().get(0), "Socks", 75, 5);

        //Second Order
        customer3.makeNewOrder("InternationalCustomer");
        customer3.addOrderItem(customer3.getOrders().get(1), "Apartment", 750_000, 1);
        customer3.addOrderItem(customer3.getOrders().get(1), "TownHouse", 650_000, 1);
        customer3.addOrderItem(customer3.getOrders().get(1), "House", 700_000, 1);
        customer3.addOrderItem(customer3.getOrders().get(1), "Farm", 4_000_000, 1);

        //Third Order
        customer3.makeNewOrder("InternationalCustomer");
        customer3.addOrderItem(customer3.getOrders().get(2), "Tata", 150_000, 1);
        customer3.addOrderItem(customer3.getOrders().get(2), "Lexus", 700_000, 1);


        // Forth Customer
        Customer customer4 = new Customer("Sam", "1", "InternationalCustomer", 2);

        //First Order
        customer4.makeNewOrder("LocalCustomer");
        customer4.addOrderItem(customer4.getOrders().get(0), "Trouser", 1_000, 1);
        customer4.addOrderItem(customer4.getOrders().get(0), "T-Shirt", 160, 3);
        customer4.addOrderItem(customer4.getOrders().get(0), "Socks", 65, 8);

        //Second Order
        customer4.makeNewOrder("InternationalCustomer");
        customer4.addOrderItem(customer4.getOrders().get(1), "Apartment", 550_000, 1);
        customer4.addOrderItem(customer4.getOrders().get(1), "TownHouse", 700_000, 1);
        customer4.addOrderItem(customer4.getOrders().get(1), "House", 900_000, 1);
        customer4.addOrderItem(customer4.getOrders().get(1), "Farm", 10_000_000, 1);

        //Third Order
        customer4.makeNewOrder("LocalCustomer");
        customer4.addOrderItem(customer4.getOrders().get(2), "Nissan", 250_000, 1);
        customer4.addOrderItem(customer4.getOrders().get(2), "Mazda", 500_000, 1);

        List<Customer> customerList = new ArrayList<Customer>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);

        printTotalValuesForEachCustomer(customerList);

        //test();
    }

    private static void test() {
        Employee.setInternationalLowDiscountValue(700_000);
        Employee.setLocalLowCustomerDuration(3);

        Customer customer = new Customer("testPiece", "999", "LocalCustomer", 3);
        customer.makeNewOrder("LocalCustomer");
        //customer.addOrderItem(customer.getOrders().get(0), "PC", 9_000, 1);
        customer.makeNewOrder("InternationalCustomer");
        customer.addOrderItem(customer.getOrders().get(1), "TownHouse", 650_000, 1);

        System.out.println(customer.getTotalValue());
    }

    private static void printTotalValuesForEachCustomer(List<Customer> customerList) {
        for (Customer customer : customerList) {
            System.out.println("For customer number: " + customer.getCustomerNum());
            System.out.println("Total Values for all orders = " + customer.getTotalValue());
        }
    }
}
