package com.psybergate.grad2021.core.oopart2.ce2a;

public class Shape {
    private int area;

    public Shape(int area) {
        this.area = area;
    }

    public int getArea() {
        return area;
    }
}
