package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;

public class LocalOrder extends Order {

    private static final String LOCAL_ORDER = "Local Order";

    public LocalOrder(String orderNum, LocalDate date, Customer customer) {
        super(orderNum, date, customer);
    }

    public double getTotalValue() {
        double totalValue = super.getTotalValue();

        if(customer.getCustomerDurationInYears() > 2 && customer.getCustomerDurationInYears() <= 5){
            return totalValue - (totalValue*0.075);
        }
        else if (customer.getCustomerDurationInYears() > 5){
            return totalValue - (totalValue*0.125);
        }
        return totalValue;
    }

    public String getOrderType() {
        return LOCAL_ORDER;
    }
}
