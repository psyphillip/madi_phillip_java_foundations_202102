package com.psybergate.grad2021.core.oopart2.demo02;

public class CurrentAccount extends Account{
    private double overdraft;

    public CurrentAccount(double balance, double overdraft) {
        super(balance);
        this.overdraft = overdraft;
    }
}
