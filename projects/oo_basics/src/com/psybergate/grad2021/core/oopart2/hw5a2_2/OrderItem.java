package com.psybergate.grad2021.core.oopart2.hw5a2_2;

public class OrderItem {
  private Product product;

  private int productQuantity;

  public OrderItem(String productName, int productQuantity) {
    this.product = ProductUtils.getProduct(productName);
    this.productQuantity = productQuantity;
    //product.toString();
  }

  public double getItemTotal() {
    System.out.println("Product name: " + product.getProductName());
    System.out.println("Product price: " + product.getProductPrice());
    return product.getProductPrice() * productQuantity;
  }
}
