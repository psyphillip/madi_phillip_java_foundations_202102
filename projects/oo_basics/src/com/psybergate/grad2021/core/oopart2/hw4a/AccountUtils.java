package com.psybergate.grad2021.core.oopart2.hw4a;

public class AccountUtils {
    public static void main(String[] args) {

        /**
         * Inheritance is when u have access to you parents members
         */

        CurrentAccount.inherited(); //Inherited

        /**
         * Static methods has the ability to hide members and are not polymorphic
         *
         * print() method in CurrentAccount was hidden
         * Can only be accessed through CurrentAccount reference type
         */

        Account account= new CurrentAccount(10000);
        account.print();
    }
}
