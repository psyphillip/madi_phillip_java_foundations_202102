package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class MoneyUtils {
    public static void main(String[] args) {
        Money money1 = new Money(new BigDecimal(10));
        Money money2 = new Money(new BigDecimal(10));

        System.out.println(money1.addMoney(money2));
    }
}
