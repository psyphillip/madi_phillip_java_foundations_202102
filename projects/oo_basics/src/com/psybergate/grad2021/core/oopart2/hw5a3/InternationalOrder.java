package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.time.LocalDate;

public class InternationalOrder extends Order {

    public static final String INTERNATIONAL_ODER = "InternationalOder";
    private static double lowDiscountValue;
    private static double highDiscountValue;
    private static double lowDiscountRate;
    private static double highDiscountRate;

    static {
        lowDiscountValue = 500_000;
        highDiscountValue = 1_000_000;
        highDiscountRate = 0.1;
        lowDiscountRate = 0.05;
    }

    public InternationalOrder() {
    }

    public InternationalOrder(String orderNum, LocalDate date) {
        super(orderNum, date);
    }

    public double getTotalValue(Customer customer) {
        double totalValue = super.getTotalValue(customer);

        if(totalValue > lowDiscountValue && totalValue <= highDiscountValue){
            return totalValue - (totalValue*lowDiscountRate);
        }

        else if(totalValue > highDiscountValue){
            return totalValue - (totalValue*highDiscountRate);
        }

        return totalValue;
    }

    public String getOrderType() {
        return INTERNATIONAL_ODER;
    }

    public static void setLowDiscountValue(double lowDiscountValue) {
        InternationalOrder.lowDiscountValue = lowDiscountValue;
    }

    public static void setHighDiscountValue(double highDiscountValue) {
        InternationalOrder.highDiscountValue = highDiscountValue;
    }

    public static void setLowDiscountRate(double lowDiscountRate) {
        InternationalOrder.lowDiscountRate = lowDiscountRate;
    }

    public static void setHighDiscountRate(double highDiscountRate) {
        InternationalOrder.highDiscountRate = highDiscountRate;
    }

    public static double getLowDiscountValue() {
        return lowDiscountValue;
    }

    public static double getHighDiscountValue() {
        return highDiscountValue;
    }

    public static double getLowDiscountRate() {
        return lowDiscountRate;
    }

    public static double getHighDiscountRate() {
        return highDiscountRate;
    }
}
