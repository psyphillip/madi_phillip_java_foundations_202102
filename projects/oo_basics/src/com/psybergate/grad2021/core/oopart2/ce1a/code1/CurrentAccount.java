package com.psybergate.grad2021.core.oopart2.ce1a.code1;

public class CurrentAccount extends Account{

    /**
     * The code wont compile because every parent constructor must be implemented/matched in the children classes
     */
    public CurrentAccount(String accountNum) {
        super(accountNum);
    }
 }
