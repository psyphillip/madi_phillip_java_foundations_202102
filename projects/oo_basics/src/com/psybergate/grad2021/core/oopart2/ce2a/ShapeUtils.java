package com.psybergate.grad2021.core.oopart2.ce2a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ShapeUtils {
    public static void main(String[] args) {

        ShapeUtils utils = new ShapeUtils();

        /**
         * primitives are passed by value
         * The value of number won't change
         */

        int number = 10;
        System.out.println("number before changeNumber() = " + number);
        changeNumber(number);
        System.out.println("number after changeNumber() = " + number);
        System.out.println();

        /**
         * Objects passed by reference
         * The state of the list will change
         */

        Shape shape = new Shape(10);
        Shape shape1 = new Shape(20);

        List<Shape> shapes = new ArrayList<Shape>();
        shapes.add(shape);
        shapes.add(shape1);

        System.out.println("Shape area before listSwap() shapes[0].area = " + shapes.get(0).getArea());
        listSwap(shapes);
        System.out.println("Shape area before listSwap() shapes[0].area = " + shapes.get(0).getArea());
        System.out.println();

        //someMethod
        System.out.println("Area of shape before someMethod = " + shape.getArea());
        utils.someMethod(shape, shape1);
        System.out.println("Area of shape before someMethod = " + shape.getArea());

    }

    private void someMethod(Shape shape, Shape shape1) {
        Shape shape2 = shape;
        shape = shape1;
        shape1 = shape2;
    }

    private static void listSwap(List<Shape> shapes) {
        Collections.swap(shapes,0,1);
    }

    private static void changeNumber(int number) {
        number *= 2;
    }
}
