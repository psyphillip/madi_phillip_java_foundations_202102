package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;

public class LocalOrder extends Order {
  private static final String LOCAL_ORDER = "Local Order";

  //private String OrderType;
  private double discount;

  public LocalOrder(String orderNum, LocalDate date, double discount) {
    super(orderNum, date);
    this.discount = discount;
  }

//    public LocalOrder(String orderNum, LocalDate date) {
//        super(orderNum, date);
//        OrderType = "Local Order";
//    }

//    public double getTotalValue(Customer customer) {
//        double totalValue = super.getTotalValue(customer);
//
//        if(customer.getCustomerDuration() > 2 && customer.getCustomerDuration() <= 5){
//            return totalValue - (totalValue*0.075);
//        }
//        else if (customer.getCustomerDuration() > 5){
//            return totalValue - (totalValue*0.125);
//        }
//        return totalValue;
//    }

  public double getTotalValue(Customer customer) {
    return super.getTotalValue(customer) * (1 - discount);
  }

  public String getOrderType() {
    return LOCAL_ORDER;
  }
}
