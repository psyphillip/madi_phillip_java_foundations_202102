package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class CurrentAccount extends Account{

    private final double MAX_OVERDRAFT;
    private double overdraft;
    private String accountType;

    public CurrentAccount(String accountNum, String balance, double MAX_OVERDRAFT, double overdraft) {
        super(accountNum, balance);
        this.MAX_OVERDRAFT = MAX_OVERDRAFT;
        this.overdraft = overdraft;
        this.accountType = "Current Account";
        System.out.println("Child constructor done");
    }


}
