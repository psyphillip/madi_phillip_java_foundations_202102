package com.psybergate.grad2021.core.oopart2.hw6a;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class OrderUtils {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String answer;
        List<Customer> customers = new ArrayList<Customer>();

        Customer customer1 = new Customer("Phillip", "5", "InternationalCustomer", 1);

        //First Order
        customer1.makeNewOrder(String.valueOf(customer1.getOrders().size()), "LocalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(0), "Trouser", 600, 5);
        customer1.addOrderItem(customer1.getOrders().get(0), "T-Shirt", 120, 3);
        customer1.addOrderItem(customer1.getOrders().get(0), "Socks", 50, 10);

        //Second Order
        customer1.makeNewOrder(String.valueOf(customer1.getOrders().size()), "LocalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(1), "Doritos", 19, 4);
        customer1.addOrderItem(customer1.getOrders().get(1), "Romany", 22, 2);
        customer1.addOrderItem(customer1.getOrders().get(1), "Maynards", 10, 3);
        customer1.addOrderItem(customer1.getOrders().get(1), "Cheppies", 1, 10);

        //Third Order
        customer1.makeNewOrder(String.valueOf(customer1.getOrders().size()), "InternationalCustomer");
        customer1.addOrderItem(customer1.getOrders().get(2), "Mitsubishi", 300_000, 1);
        customer1.addOrderItem(customer1.getOrders().get(2), "Hyundai", 250_000, 1);
        customers.add(customer1);

        myLoop:while (true) {
            System.out.println("Welcome to the e-commerce :-)");
            System.out.println();
            System.out.println("1. Login");
            System.out.println("2. New Customer");
            System.out.println("4. Employee duties");
            System.out.println("3. terminate the program");

            answer = scanner.nextLine();

            switch (answer){
                case "1":
                    String customerNum;
                    System.out.println("insert your customerNumber:");
                    customerNum = scanner.nextLine();

                    while(Integer.parseInt(customerNum) >= customers.size()){
                        System.out.println("insert your valid customerNumber:");
                        System.out.println("Or enter 0 to go back");
                        customerNum = scanner.nextLine();

                        if(customerNum.equals("0"))
                            break ;
                    }

                    loggedIn(customers.get(Integer.parseInt(customerNum)));

                    break;

                case "2":
                    Customer customer = new Customer(getCustomerName(), String.valueOf(customers.size()), getCustomerType(), getCustomerDuration());
                    System.out.println("Your customer Number is " + customers.size());
                    customers.add(customer);
                    loggedIn(customer);

                    break;

                case "3":
                    break myLoop;
            }
        }

    }

    private static void employeeLoggedIn() {

        System.out.println("You better be a valid employee :-/");
        System.out.println();

        System.out.println("1. Change Local rates");
        System.out.println("2. Change duration");
        System.out.println("3. Change order Value");
        System.out.println("4. Change International rates");
    }


    private static void loggedIn(Customer customer) {

        System.out.println("Hello " + customer.getName());

        System.out.println("1. Make new order");
        System.out.println("2. Add item to an order");
        System.out.println("3. Edit Order item");
        System.out.println("3. View all Orders");
        System.out.println("4. Log out");

        String answer = new Scanner(System.in).nextLine();

        switch (answer){
            case "1":
                int orderNum = customer.getOrders().size();
                customer.makeNewOrder(String.valueOf(orderNum), getOrderType());
                System.out.println("Your order num = " + orderNum);
                loggedIn(customer);
                break;

            case "2":
                System.out.println("Enter order number");
                String orderNumber = new Scanner(System.in).nextLine();
                while(Integer.parseInt(orderNumber) >= customer.getOrders().size()){
                    System.out.println("insert your valid Order Number:");
                    System.out.println("You can enter " + customer.getOrders().size() + " To cancel");
                    orderNumber = new Scanner(System.in).nextLine();

                    if(orderNumber.equals(String.valueOf(customer.getOrders().size()))){
                        loggedIn(customer);
                    }
                }
                customer.getOrders().get(Integer.parseInt(orderNumber)).addOrderItem(getOrderName(), getPrice(), getQuantity());
                System.out.println("Item added successfully");
                loggedIn(customer);
                break;

            case "3":
                System.out.println();
                System.out.println();
                System.out.println("Total Cost of all Orders: " + customer.getTotalValue());
                loggedIn(customer);
                break;

            case "4":
                return;

            default:
                System.out.println("Invalid answer");
                loggedIn(customer);

        }
    }

    private static double getPrice() {
        return Math.abs((double) new Random().nextInt()%100_000);
    }

    private static int getQuantity() {
        System.out.println("How many are u ordering?");
        return Integer.parseInt(new Scanner(System.in).nextLine());
    }

    private static String getOrderName() {
        System.out.println("Enter name of product");
        return new Scanner(System.in).nextLine();
    }

    private static String getOrderType() {
        String answer;

        System.out.println("Choose Policy Type");
        System.out.println();
        System.out.println("1. Local");
        System.out.println("2. International");

        answer = new Scanner(System.in).nextLine();

        if(answer.equals("1"))
            return "LocalOrder";
        else if(answer.equals("2")){
            return "InternationalOrder";
        }
        else
            return getCustomerType();
    }

    private static int getCustomerDuration() {
        System.out.println("Enter duration below:");
        return Integer.parseInt(new Scanner(System.in).nextLine());
    }

    private static String getCustomerType() {

        String answer;

        System.out.println("Choose Type");
        System.out.println();
        System.out.println("1. Local Customer");
        System.out.println("2. International Customer");

        answer = new Scanner(System.in).nextLine();

       if(answer.equals("1"))
           return "LocalCustomer";
       else if(answer.equals("2")){
           return "InternationalCustomer";
       }
       else
           return getCustomerType();
    }

    private static String getCustomerName() {
        System.out.println("Enter your name below:");
        return new Scanner(System.in).nextLine();
    }
}
