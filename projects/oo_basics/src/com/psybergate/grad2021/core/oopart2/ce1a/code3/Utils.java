package com.psybergate.grad2021.core.oopart2.ce1a.code3;

public class Utils {

    /**
     *  Through account, print could be reached because of inheritance even if it's not declared in CurrentAccount class
     *  Can the constructor that takes accountNum be inherited???? Let's find out
     *  Turns out not :-(
     */
    public static void main(String[] args) {
        Account account = new CurrentAccount();
        account.print();

        //uncomment
        //Account account1 = new CurrentAccount("12");
    }
}
