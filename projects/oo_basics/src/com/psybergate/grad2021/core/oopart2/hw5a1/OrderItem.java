package com.psybergate.grad2021.core.oopart2.hw5a1;

public class OrderItem {

    private Product product;
    private int quantity;

    public OrderItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    //Code formatting

    public double getPrice() {
        return product.getPrice();
    }

    public int getQuantity() {
        return quantity;
    }
}
