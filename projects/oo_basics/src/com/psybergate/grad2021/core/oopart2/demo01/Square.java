package com.psybergate.grad2021.core.oopart2.demo01;

public class Square extends Shape{

    /**
     * Asks what is it meant by an overriding needing a return type
     */
    //@Override
    public Square(int side1, int side2) {
        super(side1, side2);
    }

    public static void printSide1(){
        System.out.println("My parent said: Side1 is part ov every object");
    }


    public void getNameOfShape(){
        System.out.println("Square");
    }
}
