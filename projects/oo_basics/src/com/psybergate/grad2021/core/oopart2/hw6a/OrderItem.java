package com.psybergate.grad2021.core.oopart2.hw6a;

public class OrderItem {

    private int quantity;
    private Product product;

    public OrderItem(int quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    public double getPrice() {
        return product.getPrice();
    }

    public int getQuantity() {
        return quantity;
    }

    public String getOrderItemName(){
        return product.getName();
    }
}
