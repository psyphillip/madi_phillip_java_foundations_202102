package com.psybergate.grad2021.core.oopart2.demo02;

import java.util.ArrayList;
import java.util.List;

public class AccountUtils {
    public static void main(String[] args) {
        Account account1 = new CurrentAccount(1000,2000);
        Account account2 = new CurrentAccount(1000,2000);
        Account account3 = account1;

        List<Account> accounts = new ArrayList<Account>();
        accounts.add(account1);
        accounts.add(account3);
        accounts.add(account2);

        /**
         * The following statement will return false because two references points to two different Objects
         */

        System.out.println(account1 == account2);

        /**
         * The following statement will return true because two references points to the same Object
         */

        System.out.println(account3 == account1);

        /**
         * The following statement will return true because the states of the objects are the same
         */

        System.out.println(account1.equals(account3));
        System.out.println(account1.equals(new CurrentAccount(1,1)));
        /**
         * The following statement will return false because of the same reason as the first statement
         */
        System.out.println(accounts.contains(new CurrentAccount(1000, 2000)));
    }
}
