package com.psybergate.grad2021.core.oopart2.hw5a2_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProductUtils {

  private static List<Product> products = new ArrayList<Product>();

  public static Product getProduct(String productName) {

    Product product;

    if (!aProductExists()) {
      product = new Product(productName, getRandomProductPrice());
      products.add(product);
      return product;
    }

    for (int i = 0; i < products.size(); ++i) {

      if (isLastProductInTheList(i) && !theProductIsAMatch(productName, i)) {
        product = new Product(productName, getRandomProductPrice());
        products.add(product);
        return product;
      } else {
        if (theProductIsAMatch(productName, i)) {
          return products.get(i);
        }
      }
    }

    return null;
  }

  private static boolean isLastProductInTheList(int i) {
    return (i + 1) == products.size();
  }

  private static boolean theProductIsAMatch(String productName, int i) {
    return products.get(i).getProductName().equals(productName);
  }

  private static boolean aProductExists() {
    return products.size() > 0;
  }

  private static double getRandomProductPrice() {
    return (Math.abs((double) new Random().nextInt() % 100_000)) / 100;
  }

}
