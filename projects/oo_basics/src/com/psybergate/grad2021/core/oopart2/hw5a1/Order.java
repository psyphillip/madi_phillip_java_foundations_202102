package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

    private String orderNum;
    private Customer customer;
    private LocalDate orderDate;
    private List<OrderItem> orderItems = new ArrayList<OrderItem>();

    public Order(String orderNum, LocalDate orderDate) {
        this.orderNum = orderNum;
        this.orderDate = orderDate;
    }

    public void addOrderItem(String productName, double price, int quantity) {
        orderItems.add(new OrderItem(new Product(productName, price), quantity));
    }

    public double getTotalValue(Customer customer) {
        double totalValue = 0;

        for (OrderItem item : orderItems) {
            totalValue += (item.getPrice() * item.getQuantity());
        }

        return totalValue;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }
}
