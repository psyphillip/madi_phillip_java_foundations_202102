package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;

public class InternationalOrder extends Order {

  private String orderType;

  private double importDuties;

//    public InternationalOrder(String orderNum, LocalDate date) {
//        super(orderNum, date);
//        this.orderType ="InternationalOrder";
//    }

  public InternationalOrder(String orderNum, LocalDate date, double importDuties) {
    super(orderNum, date);
    this.orderType = "InternationalOder";
    this.importDuties = importDuties;
  }

//    public double getTotalValue(Customer customer) {
//        double totalValue = super.getTotalValue(customer);
//
//        if(totalValue > 500_000 && totalValue <= 1_000_000){
//            return totalValue - (totalValue*0.05);
//        }
//
//        else if(totalValue > 1_000_000){
//            return totalValue - (totalValue*0.1);
//        }
//
//        return totalValue;
//    }

  public double getTotalValue(Customer customer) {
    return super.getTotalValue(customer) + importDuties;
  }

  public String getOrderType() {
    return orderType;
  }
}
