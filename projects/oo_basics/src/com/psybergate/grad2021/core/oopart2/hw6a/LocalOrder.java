package com.psybergate.grad2021.core.oopart2.hw6a;

import java.time.LocalDate;

public class LocalOrder extends Order {

    private static final String LOCAL_ORDER = "Local Order";
    private static int lowCustomerDuration;
    private static int highCustomerDuration;
    private static double highDiscountRate;
    private static double lowDiscountRate;

    static {
        lowCustomerDuration = 2;
        highCustomerDuration = 5;
        lowDiscountRate = 0.075;
        highDiscountRate = 0.125;
    }

    public LocalOrder(String orderNum, LocalDate date) {
        super(orderNum, date);
    }

    public double getTotalValue(Customer customer) {
        double totalValue = super.getTotalValue(customer);

        if(customer.getCustomerDuration() > lowCustomerDuration && customer.getCustomerDuration() <= highCustomerDuration){
            return totalValue - (totalValue*lowDiscountRate);
        }
        else if (customer.getCustomerDuration() > highCustomerDuration){
            return totalValue - (totalValue*highDiscountRate);
        }
        return totalValue;
    }

    public String getOrderType() {
        return LOCAL_ORDER;
    }

    public static void setLowCustomerDuration(int lowCustomerDuration) {
        LocalOrder.lowCustomerDuration = lowCustomerDuration;
    }

    public static void setHighCustomerDuration(int highCustomerDuration) {
        LocalOrder.highCustomerDuration = highCustomerDuration;
    }

    public static void setHighDiscountRate(double highDiscountRate) {
        LocalOrder.highDiscountRate = highDiscountRate;
    }

    public static void setLowDiscountRate(double lowDiscountRate) {
        LocalOrder.lowDiscountRate = lowDiscountRate;
    }

    public static int getLowCustomerDuration() {
        return lowCustomerDuration;
    }

    public static int getHighCustomerDuration() {
        return highCustomerDuration;
    }

    public static double getHighDiscountRate() {
        return highDiscountRate;
    }

    public static double getLowDiscountRate() {
        return lowDiscountRate;
    }
}

