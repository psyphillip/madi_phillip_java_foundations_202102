package com.psybergate.grad2021.core.oopart2.hw4a;

public class Account {
    private int balance;

    public Account(int balance) {
        this.balance = balance;
    }

    public static void inherited(){
        System.out.println("Account's inherited static method");
    }

    public static void print(){
        System.out.println("I'm Account");
    }
}
