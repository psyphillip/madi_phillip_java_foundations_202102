package com.psybergate.grad2021.core.oopart2.hw6a;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String name;
    private String customerNum;
    private String customerType;
    private int customerDuration;
    private List<Order> orders;

    public Customer(String name, String customerNum, String customerType, int customerDuration) {
        this.name = name;
        this.customerNum = customerNum;
        this.customerType = customerType;
        this.customerDuration = customerDuration;
        orders = new ArrayList<Order>();
    }

    public double getTotalValue(){
        double totalValue = 0;

        if(orders.size() == 0){
            System.out.println("No orders placed yet");
            System.out.println();
            return 0;
        }

        for (Order order : orders) {
            double orderTotal = 0;
            System.out.println("Order number " + order.getOrderNum() + " is a/an " + order.getOrderType());
            System.out.println("there are " + order.orderList.size() + " Order Items available");
            orderTotal = order.getTotalValue(this);
            totalValue += orderTotal;
            System.out.println("Order Total: R" + orderTotal);
            System.out.println();
        }

        return totalValue;
    }

    public void addOrderItem(Order order, String name, double price, int quantity){
        order.addOrderItem(name, price, quantity);
    }

    public void makeNewOrder(String orderNum, String customerType){
        LocalDate date = LocalDate.now();

        if (customerType.equals("LocalCustomer")) {
            orders.add(new LocalOrder(orderNum, date));
        }
        else{
            orders.add(new InternationalOrder(orderNum, date));
        }
    }

    public int getCustomerDuration() {
        return customerDuration;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public String getName() {
        return name;
    }
}

