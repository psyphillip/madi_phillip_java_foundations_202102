package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String customerNum;

  private String name;

  private String customerType;

  private int customerDurationInYears;

  private List<Order> orders = new ArrayList<Order>();

  public Customer(String name, String customerNum, String customerType, int customerDuration) {
    this.name = name;
    this.customerNum = customerNum;
    this.customerType = customerType;
    this.customerDurationInYears = customerDuration;
  }

  public double getTotalValue() {
    double totalValue = 0;

    for (Order order : orders) {
      totalValue += order.getTotalValue();
    }


    return totalValue;
  }

//  public void addOrderItem(Order order, String name, double price, int quantity) {
//    order.addOrderItem(name, price, quantity);
//  }

  public Order makeNewOrder() {
    LocalDate date = LocalDate.now();
      Order order;

      if (customerType.equals("LocalCustomer")) {
        order = (new LocalOrder("123", date, this));
      }
      else{
      order = (new InternationalOrder("123", date, this));
    }
      orders.add(order);
      return order;
  }

  public int getCustomerDurationInYears() {
    return customerDurationInYears;
  }

  public List<Order> getOrders() {
    return orders;
  }

  public String getCustomerNum() {
    return customerNum;
  }
}
