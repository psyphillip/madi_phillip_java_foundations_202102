package com.psybergate.grad2021.core.oopart2.hw5a2_2;

public class Product {
  private String productName;

  private double productPrice;

  public Product(String productName, double productPrice) {
    this.productName = productName;
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public double getProductPrice() {
    return productPrice;
  }

  @Override
  public String toString() {
    return "Product{" +
        "productName='" + productName + '\'' +
        ", productPrice=" + productPrice +
        '}';
  }
}
