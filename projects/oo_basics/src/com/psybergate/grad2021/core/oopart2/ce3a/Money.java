package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public final class Money {

    private final BigDecimal Value;

    public Money(BigDecimal money) {
        this.Value = money;
    }

    public BigDecimal getValue() {
        return Value;
    }

    public BigDecimal addMoney(Money money){
        return this.getValue().add(money.getValue());
    }
}
