package com.psybergate.grad2021.core.oopart2.hw6a;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

    private String orderNum;
    private LocalDate date;
    List<OrderItem> orderList = new ArrayList<OrderItem>();

    public Order() {
    }

    public Order(String orderNum, LocalDate date) {
        this.orderNum = orderNum;
        this.date = date;
    }

    public void addOrderItem(String name, double price, int quantity){
        orderList.add(new OrderItem(quantity, new Product(name, price)));
    }

    public double getTotalValue(Customer customer){
        double totalValue = 0;

        if(orderList.size() == 0){
            System.out.println("No items in this order yet");
            System.out.println();
            return 0;
        }

        for (OrderItem item: orderList) {
            System.out.println("Name of Product: " + item.getOrderItemName() + "  Price: " + item.getPrice() + "  Quantity: " + item.getQuantity());
            totalValue += (item.getPrice() * item.getQuantity());
        }

        return totalValue;
    }

    public String getOrderType(){
        return null;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public LocalDate getDate() {
        return date;
    }
}
