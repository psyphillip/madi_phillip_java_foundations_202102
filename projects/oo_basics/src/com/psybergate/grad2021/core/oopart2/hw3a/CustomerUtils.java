package com.psybergate.grad2021.core.oopart2.hw3a;

public class CustomerUtils {
    public static void main(String[] args) {
        Customer customer = new Customer("135", "Phil");
        Customer customer1 = customer;
        Customer customer2 = new Customer("135", "Hope");


        /**
         * Two different customers with the same customerNum will be declared equal
         */
        System.out.println("customer2.equals(customer) = " + customer2.equals(customer));

        /**
         * These two are identical as they point ti the same object
         */
        System.out.println("customer1.equals(customer) = " + customer1.equals(customer));
    }
}
