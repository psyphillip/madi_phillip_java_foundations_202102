package com.psybergate.grad2021.core.oopart2.demo01;

public class Shape {
    private int side1;
    private int side2;
    private int side3;
    private double angle;

    public Shape(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public Shape(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public Shape(int side1, int side2, double angle) {
        this.side1 = side1;
        this.side2 = side2;
        this.angle = angle;
    }

    public static void printSide1(){
        System.out.println("Side1 is part ov every object");
    }
    public void getNameOfShape(){
        //Some invisible child caller
        System.out.println("Shape");
    }
}
