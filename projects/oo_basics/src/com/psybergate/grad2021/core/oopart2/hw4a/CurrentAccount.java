package com.psybergate.grad2021.core.oopart2.hw4a;

public class CurrentAccount extends Account{

    public CurrentAccount(int balance) {
        super(balance);
    }

    public static void print(){
        System.out.println("I'm CurrentAccount");
    }
}
