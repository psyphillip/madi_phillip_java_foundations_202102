package com.psybergate.grad2021.core.oopart2.ce1a.code3;

public class Account {

    private String accountNum;

    public Account() {
    }

    public Account(String accountNum) {
        this.accountNum = accountNum;
    }

    public void print(){
        System.out.println("Account print method");
    }
}
