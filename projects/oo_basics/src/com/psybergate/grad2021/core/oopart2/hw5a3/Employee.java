package com.psybergate.grad2021.core.oopart2.hw5a3;

public class Employee {

    public static void setInternationalLowDiscountValue(double value){
        if (value < InternationalOrder.getHighDiscountValue()) {
            InternationalOrder.setLowDiscountValue(value);
        }
    }

    public static void setInternationalHighDiscountValue(double value){
        if (value > InternationalOrder.getLowDiscountValue()) {
            InternationalOrder.setHighDiscountValue(value);
        }
    }

    public static void setInternationalLowDiscountRate(double rate){
        if ((rate/100) < InternationalOrder.getHighDiscountRate()) {
            InternationalOrder.setLowDiscountRate(rate/100);
        }
    }

    public static void setInternationalHighDiscountRate(double rate){
        if ((rate/100) > InternationalOrder.getLowDiscountRate()) {
            InternationalOrder.setHighDiscountRate(rate/100);
        }
    }

    public static void setLocalLowCustomerDuration(int year){
        if (year < LocalOrder.getHighCustomerDuration()) {
            LocalOrder.setLowCustomerDuration(year);
        }
    }

    public static void setLocalHighCustomerDuration(int year){
        if (year > LocalOrder.getLowCustomerDuration()) {
            LocalOrder.setHighCustomerDuration(year);
        }
    }

    public static void setLocalCustomerLowDiscountRate(double rate){
        if ((rate/100) < LocalOrder.getHighDiscountRate()) {
            LocalOrder.setLowDiscountRate(rate/100);
        }
    }

    public static void setLocalCustomerHighDiscountRate(double rate){
        if ((rate/100) > LocalOrder.getLowDiscountRate()) {
            LocalOrder.setHighDiscountRate(rate/100);
        }
    }
}
