package com.psybergate.grad2021.core.oopart1.hw5a;

public class SavingsAccount extends Account{

    private double miniBalance;

    public SavingsAccount(String accNum, double balance, double miniBalance) {
        super(accNum, balance);
        this.miniBalance = miniBalance;
    }
    
    public void print(){
        super.print();
        System.out.println("miniBalance = " + miniBalance);
    }
}
