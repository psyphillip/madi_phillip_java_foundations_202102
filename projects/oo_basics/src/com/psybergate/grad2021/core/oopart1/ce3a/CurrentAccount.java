package com.psybergate.grad2021.core.oopart1.ce3a;

public class CurrentAccount extends Account{


    public CurrentAccount(int accountNum, String name, String surname, double balance) {
        super(accountNum, name, surname, balance);
    }

    @Override
    public String getAccountType() {
        return "CurrentAccount";
    }
}
