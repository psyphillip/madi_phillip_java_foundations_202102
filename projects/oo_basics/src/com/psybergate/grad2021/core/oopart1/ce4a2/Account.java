package com.psybergate.grad2021.core.oopart1.ce4a2;

public abstract class Account {

    private int accountNum;
    private String name;
    private String surname;
    private double balance;

    public Account(int accountNum, String name, String surname, double balance) {
        this.accountNum = accountNum;
        this.name = name;
        this.surname = surname;
        this.balance = balance;
    }

    public abstract String getAccountType();

    public double getBalance() {
        return balance;
    }

    public int getAccountNum() {
        return accountNum;
    }
}

