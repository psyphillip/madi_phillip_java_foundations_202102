package com.psybergate.grad2021.core.oopart1.ce4a;

public class Customer {
    public static void main(String[] args) {
        /**
         * The code compiles using the same reference pointing to two different object types
         */
        Account account1 = new CurrentAccount(1,50000.0,"CurrentAccount", 2000.0);
        Account account2 = new SavingsAccount(2,20000.0,"SavingsAccount", 5000.0);

        /**
         * Running the following lines provides different results regardless of having the same reference type
         */
        System.out.println("account1 AccountType = " + account1.getAccountType());
        System.out.println("account2 AccountType = " + account2.getAccountType());
    }
}
