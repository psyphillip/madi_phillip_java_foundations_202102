package com.psybergate.grad2021.core.oopart1.ce3a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    public static void main(String[] args) {
        List<Account> accounts = new ArrayList<Account>();
        Account account = new CurrentAccount(1,"Phill","Madi",50000.0);
        accounts.add(account);
        account = mutateObject(accounts.get(0));
        for (Account account1 : accounts) {
            account1 = account;
        }
    }

    private static Account mutateObject(Account account) {
        account.setBalance(6000.0);
        return account;
    }
}
