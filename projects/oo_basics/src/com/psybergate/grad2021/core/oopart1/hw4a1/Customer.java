package com.psybergate.grad2021.core.oopart1.hw4a1;

public class Customer {
    private static int minAge = 18;

    public static int getMinAge(){
        return minAge;
    }

    public static void setMinAge(int age){
        minAge = age;
    }

    private String custNum;
    private String name;
    private String surname;
    private String custType;

    public Customer(String custNum, String name, String surname, String custType) {
        this.custNum = custNum;
        this.name = name;
        this.surname = surname;
        this.custType = custType;
    }

    public String getCustNum() {
        return custNum;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }
}
