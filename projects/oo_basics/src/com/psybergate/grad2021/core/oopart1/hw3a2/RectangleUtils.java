package com.psybergate.grad2021.core.oopart1.hw3a2;

import com.psybergate.grad2021.core.oopart1.hw3a1.Rectangle;

public class RectangleUtils {
    public static void main(String[] args) {

        /**
         * To avoid code duplication, the rectangle class is imported from the previous home work
         */

        Rectangle rectangle1 = new Rectangle(50,60);
        Rectangle rectangle2 = new Rectangle(40,60);
        Rectangle rectangle3 = new Rectangle(50,70);

        /**
         * Similarity
         */
        System.out.println("rectangle1.getMaxLength() = " + rectangle1.getMaxLength());
        System.out.println("rectangle2.getMaxLength() = " + rectangle2.getMaxLength());
        System.out.println("rectangle3.getMaxLength() = " + rectangle3.getMaxLength());
        System.out.println();

        /**
         * difference
         */
        System.out.println("rectangle1.getArea() = " + rectangle1.getArea());
        System.out.println("rectangle1.getArea() = " + rectangle2.getArea());
        System.out.println("rectangle1.getArea() = " + rectangle3.getArea());
        System.out.println();

        Rectangle.setMaxLength(250);
        Rectangle.setMaxWidth(150);
        //Changes for every object

        System.out.println("rectangle1.getMaxLength() = " + rectangle1.getMaxLength());
        System.out.println("rectangle2.getMaxLength() = " + rectangle2.getMaxLength());
        System.out.println("rectangle3.getMaxLength() = " + rectangle3.getMaxLength());
        System.out.println();
    }
}
