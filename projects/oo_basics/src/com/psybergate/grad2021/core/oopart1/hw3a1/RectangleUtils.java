package com.psybergate.grad2021.core.oopart1.hw3a1;

public class RectangleUtils {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(100, 50);
        System.out.println("rectangle.getArea() = " + rectangle.getArea());
        Rectangle rectangle1 = new Rectangle(200, 100);
        System.out.println("rectangle1.getArea() = " + rectangle1.getArea());
    }
}