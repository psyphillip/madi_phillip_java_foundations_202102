package com.psybergate.grad2021.core.oopart1.ce4a2;

public class CurrentAccount extends Account{

    private final String accountType;
    private static final double MAX_OVERDRAFT = 100_000.0;

    public CurrentAccount(int accountNum, String name, String surname, double balance, String accountType) {
        super(accountNum, name, surname, balance);
        this.accountType = accountType;
    }

    @Override
    public String getAccountType() {
        return accountType;
    }

    public boolean needsToBeReviewed(){
        if(isOverDrawn() || getBalance()<-50_000){
            return true;
        }
        return false;
    }

    public boolean isOverDrawn(){
        return getBalance() < 0;
    }
}
