package com.psybergate.grad2021.core.oopart1.hw5a;

public class CurrentAccount extends Account{

    private double overdraft;

    public CurrentAccount(String accNum, double balance, double overdraft) {
        super(accNum, balance);
        this.overdraft = overdraft;
    }
    
    public void print(){
        super.print();
        System.out.println("overdraft = " + overdraft);
    }
}
