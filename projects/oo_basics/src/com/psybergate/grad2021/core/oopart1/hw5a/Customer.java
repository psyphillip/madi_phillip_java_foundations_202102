package com.psybergate.grad2021.core.oopart1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String customerNumber;
    private String name;
    private String surname;

    List<Account> accounts = new ArrayList<Account>();

    public Customer(String custNum, String name, String surname) {
        this.customerNumber = custNum;
        this.name = name;
        this.surname = surname;
    }

    public void addAccount(Account account){
        accounts.add(account);
    }

    public void printBalances(){
        for (Account account : this.accounts) {
            account.print();
        }
    }
}
