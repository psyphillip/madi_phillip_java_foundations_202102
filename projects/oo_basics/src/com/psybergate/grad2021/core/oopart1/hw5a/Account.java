package com.psybergate.grad2021.core.oopart1.hw5a;

public abstract class Account {

    protected String accountNumber;
    protected double balance;

    public Account(String accNum, double balance) {
        this.accountNumber = accNum;
        this.balance = balance;
    }

    public void print(){
        System.out.print("account.accNum + \"/\" + account.balance = " + "/" + accountNumber + "/" + balance + " ");
    }
}
