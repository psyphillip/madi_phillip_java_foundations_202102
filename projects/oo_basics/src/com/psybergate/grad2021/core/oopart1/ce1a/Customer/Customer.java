package com.psybergate.grad2021.core.oopart1.ce1a.Customer;

import com.psybergate.grad2021.core.oopart1.ce1a.CurrentAccount.CurrentAccount;
import java.util.ArrayList;
import java.util.List;

public class Customer {
    private int custNum;
    private String name;
    private String address;
    List<CurrentAccount> currentAccountList = new ArrayList<CurrentAccount>();

    public Customer(int custNum, String name, String address) {
        this.custNum = custNum;
        this.name = name;
        this.address = address;
    }

    public void addAccount(CurrentAccount currentAccount){
        currentAccountList.add(currentAccount);
    }

    public double getTotal(){
        double totalBalance = 0;
        for (CurrentAccount currentAccount : currentAccountList) {
            totalBalance += currentAccount.getBalance();
        }
        return totalBalance;
    }

}
