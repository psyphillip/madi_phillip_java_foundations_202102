package com.psybergate.grad2021.core.oopart1.ce3b;

public class ClassB extends ClassA{

    int num;
    String name;

    public ClassB(int number) {
        super(number);
        num = -1;
        name = "Phill";
    }

    public ClassB(int number, int num) {
        this(number,num, "Phill");
//        this.num = num;
//        name = "Phill";
    }
    public ClassB(int number, int num, String name){
        super(number);
        this.num = num;
        this.name = name;
    }

    public int doMultiply(int i){
        return i*this.num;
        //System.out.println("Passed i = " + i);
    }

    public int doSomething(int i){
        return super.doSomething(i);
    }
    
    public String doToString(int i){
        String s = Integer.toString(i);
        return s;
    }
}
