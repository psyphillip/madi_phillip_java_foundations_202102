package com.psybergate.grad2021.core.oopart1.ce4a2;

public class SavingsAccount extends Account{

    private final String accountType;
    private static final double MIN_BALANCE = 5000.0;

    public SavingsAccount(int accountNum, String name, String surname, double balance, String accountType) {
        super(accountNum, name, surname, balance);
        this.accountType = accountType;
    }

    @Override
    public String getAccountType() {
        return accountType;
    }

    public boolean needsToBeReviewd(){
        if(getBalance() < 2000){
            return true;
        }
        return false;
    }

    public boolean isOverdrawn(){
        if(getBalance() < 5000){
            return true;
        }
        return false;
    }
}
