package com.psybergate.grad2021.core.oopart1.ce2a;

public class CurrentAccount extends Account{

    private final String accountType;
    private double intrestRate;
    private double overDraft;
    private static final double MAX_OVERDRAFT = 100_000.0;

    public CurrentAccount(int accountNum, String name, String surname, double balance, String accountType, double intrestRate, double overDraft) {
        super(accountNum, name, surname, balance);
        this.accountType = accountType;
        this.intrestRate = intrestRate;
        this.overDraft = overDraft;
    }

    @Override
    public String getAccountType() {
        return accountType;
    }

    public boolean needsToBeReviewed(){
        if(isOverDrawn() || getBalance()<-50_000){
            return true;
        }
        return false;
    }

    public boolean isOverDrawn(){
        return getBalance() < 0;
    }
}
