package com.psybergate.grad2021.core.oopart1.ce3b;

public class ClassA {
    int number;

    public ClassA(int number) {
        this.number = number;
    }

    public int doSomething(int i){
        return i*2;
    }

    @Override
    public String toString(){
        return "ClassA to String method";
    }
}
