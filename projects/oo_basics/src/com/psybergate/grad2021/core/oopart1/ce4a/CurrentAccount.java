package com.psybergate.grad2021.core.oopart1.ce4a;

public class CurrentAccount extends Account{

    private double overdraft;

    public CurrentAccount(int accountNum, double balance, String accountType, double overdraft) {
        super(accountNum, balance,accountType);
        this.accountType = accountType;
        this.overdraft = overdraft;
    }

    public String getAccountType() {
        return accountType;
    }
}
