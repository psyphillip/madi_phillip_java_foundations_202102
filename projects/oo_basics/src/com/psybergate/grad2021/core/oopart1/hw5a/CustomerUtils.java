package com.psybergate.grad2021.core.oopart1.hw5a;

public class CustomerUtils {

    public static void main(String[] args) {
        Customer customer1 = new Customer("1", "Phil", "Madi");

        customer1.addAccount(new CurrentAccount("1",5000.0,2000));
        customer1.addAccount(new CurrentAccount("2",6000.0,2500));
        customer1.addAccount(new SavingsAccount("3",2000.0,1000));
        customer1.addAccount(new SavingsAccount("4",9000.0,1500));

        customer1.printBalances();

    }
}
