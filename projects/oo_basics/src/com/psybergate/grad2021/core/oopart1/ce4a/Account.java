package com.psybergate.grad2021.core.oopart1.ce4a;

public class Account {

    int accountNum;
    double balance;
    String accountType;

    public Account(int accountNum, double balance, String accountType) {
        this.accountNum = accountNum;
        this.balance = balance;
        this.accountType = accountType;
    }

    public String getAccountType() {
        return accountType;
    }
}
