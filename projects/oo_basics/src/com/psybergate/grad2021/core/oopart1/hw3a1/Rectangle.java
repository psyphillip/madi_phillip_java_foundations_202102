package com.psybergate.grad2021.core.oopart1.hw3a1;

public class Rectangle {

    private static int MAX_LENGTH;
    private static int MAX_WIDTH;
    private static int MAX_AREA;

    static {
        MAX_LENGTH = 200;
        MAX_WIDTH = 100;
        MAX_AREA = 15000;
    }

    public static void setMaxLength(int maxLength) {
        MAX_LENGTH = maxLength;
    }

    public static void setMaxWidth(int maxWidth) {
        MAX_WIDTH = maxWidth;
    }

    public static void setMaxArea(int maxArea) {
        MAX_AREA = maxArea;
    }

    public static int getMaxLength(){
        return MAX_LENGTH;
    }

    public static int getMaxWidth(){
        return MAX_WIDTH;
    }
    public static int getMaxArea(){
        return MAX_AREA;
    }

    private int length;
    private int width;
    private int area;

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
        area = calcArea(length, width);
        if(!isValid()){
            throw new RuntimeException("Constraints breached");
        }
    }

    private boolean isValid() {
        return (length < MAX_LENGTH && width < MAX_WIDTH && area < MAX_AREA);
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public int getArea() {
        return area;
    }

    private int calcArea(int length, int width) {
        return length*width;
    }
}
