package com.psybergate.grad2021.core.oopart1.hw4a2;

import com.psybergate.grad2021.core.oopart1.hw4a1.Customer;

public class CustomerUtils {

    public static void main(String[] args) {
        Customer customer1 = new Customer("1", "Phil","Madi","Person");
        Customer customer2 = new Customer("2", "Karabo","Mahome","Business");
        Customer customer3 = new Customer("3", "Ben","Martins","Person");

        /**
         * Similarities
         */
        System.out.println("customer1.getMinAge() = " + customer1.getMinAge());
        System.out.println("customer2.getMinAge() = " + customer2.getMinAge());
        System.out.println("customer3.getMinAge() = " + customer3.getMinAge());
        System.out.println();

        /**
         * differences
         */
        System.out.println("customer1.getCustNum() = " + customer1.getCustNum());
        System.out.println("customer2.getCustNum() = " + customer2.getCustNum());
        System.out.println("customer3.getCustNum() = " + customer3.getCustNum());
    }
}
