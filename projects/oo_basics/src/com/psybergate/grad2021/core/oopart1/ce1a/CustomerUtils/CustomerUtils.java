package com.psybergate.grad2021.core.oopart1.ce1a.CustomerUtils;

import com.psybergate.grad2021.core.oopart1.ce1a.CurrentAccount.CurrentAccount;
import com.psybergate.grad2021.core.oopart1.ce1a.Customer.Customer;

public class CustomerUtils {
    public static void main(String[] args) {
        CustomerUtils customerUtils = new CustomerUtils();
        customerUtils.test();
    }

    public void test() {
        Customer phil = new Customer(1, "Phill", "Psybergate");
        phil.addAccount(new CurrentAccount("1", 5800.94));
        phil.addAccount(new CurrentAccount("2", 600.00));

        System.out.println("phil.getTotal() = " + phil.getTotal());
    }
}
