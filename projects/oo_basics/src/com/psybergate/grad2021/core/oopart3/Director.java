package com.psybergate.grad2021.core.oopart3;

public class Director extends Employee{

  public Director(String employeeNum, String name, String surname, double annualSalary) {
    super(employeeNum, name, surname, annualSalary);
  }

  public double getMonthlyMedicalAidContribution() {
    return 5000;
  }
}
