package com.psybergate.grad2021.core.oopart3;

public class Manager extends Employee{

  private static double monthlyMedicalAidContributionRate = 0.075;

  private static double minimumContribution = 1200;

  private static double maximumContribution = 3000;


  public Manager(String employeeNum, String name, String surname, double annualSalary) {
    super(employeeNum, name, surname, annualSalary);
  }

  public double getMonthlyMedicalAidContribution() {

    if (monthlySalary() * monthlyMedicalAidContributionRate < minimumContribution) {
      return minimumContribution;
    } else if (monthlySalary() * monthlyMedicalAidContributionRate > maximumContribution) {
      return maximumContribution;
    }

    return monthlySalary() * monthlyMedicalAidContributionRate;
  }

  private double monthlySalary() {
    return annualSalary / 12;
  }
}
