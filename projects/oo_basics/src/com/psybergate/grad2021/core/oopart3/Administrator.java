package com.psybergate.grad2021.core.oopart3;

public class Administrator extends Employee {

  private static double monthlyMedicalAidContributionRate = 0.1;

  private static double minimumContribution = 800;

  private static double maximumContribution = 2000;

  public Administrator(String employeeNum, String name, String surname, double annualSalary) {
    super(employeeNum, name, surname, annualSalary);
  }

  public double getMonthlyMedicalAidContribution() {

    if (monthlySalary() * monthlyMedicalAidContributionRate < minimumContribution) {
      return minimumContribution;
    } else if (monthlySalary() * monthlyMedicalAidContributionRate > maximumContribution) {
      return maximumContribution;
    }

    return monthlySalary() * monthlyMedicalAidContributionRate;
  }

  private double monthlySalary() {
    return annualSalary / 12;
  }
}
