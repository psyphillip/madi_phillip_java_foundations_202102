package com.psybergate.grad2021.core.oopart3;

public class Employee {

  private String employeeNum;

  private String name;

  private String surname;

  protected double annualSalary;

  public Employee(String employeeNum, String name, String surname, double annualSalary) {
    this.employeeNum = employeeNum;
    this.name = name;
    this.surname = surname;
    this.annualSalary = annualSalary;
  }

  @Override
  public String toString() {
    return "Employee{" +
        "employeeNum='" + employeeNum + '\'' +
        ", name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", annualSalary=" + annualSalary +
        '}';
  }
}
