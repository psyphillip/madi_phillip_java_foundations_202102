package com.psybergate.grad2021.core.innerclasses.objectinnerclass;

public class Product {

  private String productName;

  private double productPrice;

  private Supplier supplier = new Supplier("evetech", "queries@evetech.co.za");

  /**
   * In this context, every product belongs to ta different supplier
   */
  class Supplier {

    private String supplierName;

    private String supplierEmail;

    public Supplier(String supplierName, String supplierEmail) {
      this.supplierName = supplierName;
      this.supplierEmail = supplierEmail;
    }

    public String getSupplierName() {
      return supplierName;
    }

    public void setSupplierName(String supplierName) {
      this.supplierName = supplierName;
    }

    public String getSupplierEmail() {
      return supplierEmail;
    }

    public void setSupplierEmail(String supplierEmail) {
      this.supplierEmail = supplierEmail;
    }
  }

  public Supplier getSupplier() {
    return supplier;
  }

  public Product(String productName, double productPrice) {
    this.productName = productName;
    this.productPrice = productPrice;
  }
}
