package com.psybergate.grad2021.core.innerclasses.objectinnerclass;

public class ObjectInnerClassClient {
  public static void main(String[] args) {

    Product product = new Product("gaming PC", 15_000);
    Product product1 = new Product("gaming keyboard", 600);

    //Printing supplier for each product
    getSupplierDetails(product);
    getSupplierDetails(product1);

    //Change supplier details on one product and u change for all of em

    product.getSupplier().setSupplierName("Dell");
    product.getSupplier().setSupplierEmail("enquiries@dell.co.za");

    //reprint supplier details
    getSupplierDetails(product);
    getSupplierDetails(product1);

  }

  private static void getSupplierDetails(Product product) {
    System.out.println("Supplier: " + product.getSupplier().getSupplierName());
    System.out.println("Email: " + product.getSupplier().getSupplierEmail());
  }
}
