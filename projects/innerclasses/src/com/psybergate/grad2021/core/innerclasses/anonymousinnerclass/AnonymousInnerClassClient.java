package com.psybergate.grad2021.core.innerclasses.anonymousinnerclass;

public class AnonymousInnerClassClient {

  public static void main(String[] args) {
    getSupplierDetails(new Product() {

      String productName = "Gaming mouse";

      double productPrice = 250;

      /**
       * In this context, every product belongs to the same supplier
       */
      Supplier supplier = new Supplier("evetech", "queries@evetech.co.za");

      class Supplier {

        private String supplierName;

        private String supplierEmail;

        public Supplier(String supplierName, String supplierEmail) {
          this.supplierName = supplierName;
          this.supplierEmail = supplierEmail;
        }

        public String getSupplierName() {
          return supplierName;
        }

        public void setSupplierName(String supplierName) {
          this.supplierName = supplierName;
        }

        public String getSupplierEmail() {
          return supplierEmail;
        }

        public void setSupplierEmail(String supplierEmail) {
          this.supplierEmail = supplierEmail;
        }

      }
    });
  }

  public static void getSupplierDetails(Product product) {
//    System.out.println("Supplier: " + product.().getSupplierName());
//    System.out.println("Email: " + product.getSupplier().getSupplierEmail());
  }
}
