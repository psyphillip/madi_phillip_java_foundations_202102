package com.psybergate.grad2021.core.strings.hw1a;

public class StringFormatting {

  public static void main(String[] args) {
    basicStringFormatting();
    multipleParametersStringFormatting();
    reusingParametersStringFormatting();
  }

  private static void basicStringFormatting() {

    System.out.printf("Hi there, my name is %s\n", getName());

  }

  private static void multipleParametersStringFormatting() {

    System.out.printf("Hi there, my name is %s and I am %d years old. Unfortunately I have R%f in my bank account\n", getName(), getAge(), getBankBalance());

  }

  private static void reusingParametersStringFormatting() {
    System.out.printf("Hi there, my name is %s and I am %d years old. Unfortunately I have R%f in my bank account. Just imagine with R%3$f, totally terrifying\n", getName(), getAge(), getBankBalance());

  }

  private static String getName() {
    return "Phillip";
  }

  private static int getAge() {
    return  24;
  }

  public static double getBankBalance() {
    return 13.98;
  }
}
