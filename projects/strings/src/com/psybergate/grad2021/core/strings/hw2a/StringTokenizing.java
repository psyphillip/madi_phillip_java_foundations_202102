package com.psybergate.grad2021.core.strings.hw2a;

import java.util.StringTokenizer;

public class StringTokenizing {

  private static StringTokenizer stringTokenizer;

  public static void main(String[] args) {
     String word = "Phil is just another focused beast";
     String wordWithDelimeter = "Phil;is,just&another;focused,beast";
    String delimeter = ";,& ";

    defaultTokenizing(word);
    tokenizeWithADelimeter(wordWithDelimeter, delimeter);
  }

  private static void tokenizeWithADelimeter(String wordWithDelimeter, String delimeter) {
    System.out.println();
    System.out.println(String.format("Tokenizing %s with %s delimeters \n", wordWithDelimeter,delimeter));

    stringTokenizer = new StringTokenizer(wordWithDelimeter, delimeter);

    while (stringTokenizer.hasMoreTokens()) {
      System.out.println(stringTokenizer.nextToken());
    }
  }

  private static void defaultTokenizing(String word) {
    System.out.println(String.format("Default Tokenizing: %s", word));

    StringTokenizer stringTokenizer = new StringTokenizer(word);

    while(stringTokenizer.hasMoreTokens()){
      System.out.println(stringTokenizer.nextToken());
    }
  }

}
