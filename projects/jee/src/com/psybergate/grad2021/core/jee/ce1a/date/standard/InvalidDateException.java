package com.psybergate.grad2021.core.jee.ce1a.date.standard;

public class InvalidDateException extends Exception{

  public InvalidDateException(String message) {
    super(message);
  }
}
