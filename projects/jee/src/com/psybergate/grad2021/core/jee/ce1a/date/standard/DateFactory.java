package com.psybergate.grad2021.core.jee.ce1a.date.standard;

public interface DateFactory {

  Date createDate(int year, int month, int day) throws InvalidDateException;
}
