package com.psybergate.grad2021.core.jee.ce1a.date.standard;

public interface Date {

  int getYear();

  int getMonth();

  int getDay();

  boolean isLeapYear();

  Date addDays(int numDays) throws InvalidDateException;

}
