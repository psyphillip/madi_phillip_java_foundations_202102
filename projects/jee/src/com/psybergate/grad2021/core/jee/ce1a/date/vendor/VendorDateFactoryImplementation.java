package com.psybergate.grad2021.core.jee.ce1a.date.vendor;

import com.psybergate.grad2021.core.jee.ce1a.date.standard.Date;
import com.psybergate.grad2021.core.jee.ce1a.date.standard.DateFactory;
import com.psybergate.grad2021.core.jee.ce1a.date.standard.InvalidDateException;

public class VendorDateFactoryImplementation implements DateFactory {
  @Override
  public Date createDate(int year, int month, int day) throws InvalidDateException {
    return new VendorDateImplementation(year, month, day);
//    return new VendorDateImplementation(year, month, day);
  }
}
