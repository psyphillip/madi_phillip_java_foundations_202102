package com.psybergate.grad2021.core.jee.ce1a.date.vendor;

import com.psybergate.grad2021.core.jee.ce1a.date.standard.Date;
import com.psybergate.grad2021.core.jee.ce1a.date.standard.DateFactory;
import com.psybergate.grad2021.core.jee.ce1a.date.standard.InvalidDateException;

public class VendorDateImplementation implements Date {

  private int year;

  private int month;

  private int day;

  public VendorDateImplementation(int year, int month, int day) throws InvalidDateException {

    if (isInvalidMonth(month)) {
      throw new InvalidDateException("Invalid Month");
    }

    if (isInvalidDay(day)) {
      throw new InvalidDateException("Invalid Day");
    }

    this.year = year;
    this.month = month;
    this.day = day;
  }

  private boolean isInvalidDay(int day) {

    if (day > 31) {
      return true;
    } else if (getMonth() == 2) {
      if (isLeapYear()) {
        return day > 29;
      } else {
        return day > 28;
      }
    } else if (getMonth() == 4 || getMonth() == 6 || getMonth() == 9 || getMonth() == 11) {
      return day > 30;
    }

    return false;
  }

  private boolean isInvalidMonth(int month) {
    return month < 0 || month > 12;
  }

  @Override
  public int getYear() {
    return year;
  }

  @Override
  public int getMonth() {
    return month;
  }

  @Override
  public int getDay() {
    return day;
  }

  @Override
  public boolean isLeapYear() {
    if (getYear() % 400 == 0) {
      return true;
    } else if (getYear() % 4 == 0) {
      return true;
    }
    return false;
  }

  @Override
  public Date addDays(int numDays) throws InvalidDateException {
    day += numDays;
    while (isInvalidDay(day)) {
      adjustDate();
    }
    return new VendorDateImplementation(getYear(), getMonth(), getDay());
  }

  private void adjustDate() {
    if (month == 12) {
      ++year;
      month = 1;
      day -= 31;
    } else if (month == 2) {
      if (isLeapYear()) {
        month++;
        day -= 29;
      } else {
        month++;
        day -= 28;
      }
    } else if (getMonth() == 4 || getMonth() == 6 || getMonth() == 9 || getMonth() == 11) {
      ++month;
      day -= 31;
    } else {
      month++;
      day -= 30;
    }

  }
}
