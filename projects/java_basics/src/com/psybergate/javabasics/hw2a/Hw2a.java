package com.psybergate.javabasics.hw2a;

/**
 * The static variables and initializers executes when the class is loaded on the virtual machine just the program runs
 *
 * When the program runs, the first method being executed is the main method
 *
 * To prove this, statements will be output and the order shall reveal the order of execution
 */

public class Hw2a {

    static int value = 10;

    public Hw2a(int value) {
        this.value = value;
    }
    static {
        System.out.println("value = " + value);
        value = 4;
        System.out.println("value = " + value);
    }

    public static void main(String[] args) {
        System.out.println("value = " + new Hw2a(30).value);
    }
}
