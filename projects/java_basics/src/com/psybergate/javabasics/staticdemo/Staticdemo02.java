package com.psybergate.javabasics.staticdemo;

class Vehicle {
    public Vehicle() {
        System.out.println("Vehicle Constructor summoned");
    }

    public static void kmToMiles(int km) {
        System.out.println("Inside the parent class / static method");
    }
}

class Car extends Vehicle {
    public Car() {
    }

    public static void kmToMiles(int km) {
        System.out.println("Inside the child class / static method");
    }
}

public class Staticdemo02 {

    public static void main(String[] args) {

        Car v = new Car();
        //Vehicle.kmToMiles(10);
    }
}