package com.psybergate.javabasics.hw5a;

import com.psybergate.javabasics.hw5a.Hw5a;


public class Hw5b {
    public static void main(String[] args) {
        Hw5a hw5a = new Hw5a();
        //hw5a.privateMethod();
        hw5a.defaultMethod();
        hw5a.protectedMethod();
        hw5a.publicMethod();
    }
}