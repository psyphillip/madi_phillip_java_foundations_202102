package com.psybergate.javabasics.hw5a;

public class Hw5a {

    private static void privateMethod(){
        System.out.println("Reached a private Method");
    }

    static void defaultMethod(){
        System.out.println("Reached a default Method");
    }

    protected static void protectedMethod(){
        System.out.println("Reached a protected Method");
    }

    public static void publicMethod(){
        System.out.println("Reached a public method");
    }

    public static void main(String[] args) {
    }
}