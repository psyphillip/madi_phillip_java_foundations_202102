package com.psybergate.javabasics.hw5b;

import com.psybergate.javabasics.hw5a.Hw5a;

public class Hw5b extends Hw5a{
    public static void main(String[] args) {
        //privateMethod();
        //defaultMethod();
        protectedMethod();
        publicMethod();
    }
}
