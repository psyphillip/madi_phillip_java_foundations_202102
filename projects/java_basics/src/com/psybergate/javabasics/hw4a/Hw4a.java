package com.psybergate.javabasics.hw4a;

public class Hw4a {
    public static void main(String[] args) {

        // % operator
        System.out.println("% operator:");
        System.out.println("the remainder between 2 / 2 = " + 2 % 2);
        System.out.println("the remainder between 5 / 2 = " + 5 % 2);
        System.out.println();


        // pre/post ++ operator
        System.out.println("pre/post ++ operator:");
        int num = 0, num1 = 0;
        System.out.println("++num = " + ++num);
        System.out.println("num1++ = " + num1++);
        System.out.println();


        // == operator
        System.out.println("== operator");
        System.out.println("is 2 = equal to 2? -> " + (2 == 2));
        System.out.println("is 2 = equal to 4? -> " + (2 == 4));
        System.out.println();


        // && and & operators
        System.out.println("&& and & operators");
        System.out.println("false and true");
        if(!firstCondition() && secondCondition()){
            System.out.println("Both conditions executed");
        }
        if(!firstCondition() & secondCondition()){
            System.out.println("Both conditions executed");
        }
        System.out.println();


        // || and | operators
        System.out.println("|| and | operators");
        System.out.println("true and false");
        if (firstCondition() || !secondCondition()) {
            System.out.println("Both conditions executed");
        }
        if (firstCondition() | !secondCondition()) {
            System.out.println("Both conditions executed");
        }
        System.out.println();


        // += operator
        System.out.println("+= operator");
        num = 0;
        System.out.println("num + 5 = " + (num += 5));
        System.out.println();


        // ternary operator
        System.out.println("Ternary Operator");
        String result = (1 == 1) ? "true" : "false";
        System.out.println("is 1 equal to 1? -> " + result);
        result = (4 == 1) ? "true" : "false";
        System.out.println(" is 4 equal to 1? -> " + result);
        System.out.println();

    }

    private static boolean secondCondition() {
        System.out.println("Second condition executed");
        return true;
    }

    private static boolean firstCondition() {
        System.out.println("First condition executed");
        return true;
    }
}
