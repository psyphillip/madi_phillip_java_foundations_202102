package com.psybergate.javabasics.hw6a;

class Outer{
    static int staticInteger = 5;
    int nonStaticInteger = 4;

    public static void staticMethod(){
        nonStaticInteger = 9; // Static methods can't alter object variables
        System.out.println("Reached a static method");
    }

    public void nonStaticMethod(){
        System.out.println("Reached a non Static Method");
    }

    static class StaticClass{
        String staticMember;

        public void method(){
            staticMethod();
            nonStaticMethod(); //Error because static members can't invoke non static methods (Called from static class)
        }

        public String getStaticMember() {
            return staticMember;
        }

        public void setStaticMember(String staticMember) {
            this.staticMember = staticMember;
        }
    }
}

public class Hw6a {
    public static void main(String[] args) {
        Outer.StaticClass staticClass = new Outer.StaticClass();
        Outer outer = new Outer();
        System.out.println(staticClass.staticMember);
        System.out.println(Outer.StaticClass.staticMember);// staticMember can't be referenced without a class instance since it's an object variable and not a static variable
        Outer.staticMethod();
        Outer.nonStaticMethod(); // Need an instance to be invoked
        outer.nonStaticMethod();
    }
}
