package com.psybergate.javabasics.hw1a;

public class Hw1a {
    public static void main(String[] args) {
        Hw1b hw1b = new Hw1b("You've found me");
        System.out.println("hw1b = " + hw1b.getSecondClass());
    }
}

class Hw1b {

    String secondClass;

    public Hw1b(){

    }

    public Hw1b(String secondClass) {
        this.secondClass = secondClass;
    }

    public void setSecondClass(String secondClass) {
        this.secondClass = secondClass;
    }

    public String getSecondClass() {
        return secondClass;
    }
}
