package com.psybergate.grad2021.core.exceptions.hw4b;

public class InvalidWithdrawalException extends RuntimeException {

  public InvalidWithdrawalException() {
    System.out.println("Withdrawal amount is more than the available balance");
  }

}