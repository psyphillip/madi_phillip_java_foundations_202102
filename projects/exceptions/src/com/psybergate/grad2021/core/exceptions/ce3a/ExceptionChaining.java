package com.psybergate.grad2021.core.exceptions.ce3a;

public class ExceptionChaining {

  public static void main(String[] args) {
    printStacktraceOfExceptionChain();
  }

  private static void printStacktraceOfExceptionChain() {
    try {
      secondException();
    }catch (RuntimeException runtimeException){
      runtimeException.printStackTrace();
    }
  }

  public static void secondException() {
    try {
      firstException();
    }catch (Exception exception){
      throw new RuntimeException(exception);
    }
  }

  public static void firstException() throws Exception {
    throw new Exception();
  }
}
