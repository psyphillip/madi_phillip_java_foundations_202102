package com.psybergate.grad2021.core.exceptions.hw4b;

public class Account {

  private String accountNum;

  private String accountHolderName;

  private double balance;

  public Account(String accountNum, String accountHolderName, double balance) {
    this.accountNum = accountNum;
    this.accountHolderName = accountHolderName;
    this.balance = balance;
  }

  public void deposit(double amount) {
    balance += amount;
  }

  public void withdraw(double amount) {
    balance -= amount;
  }

  public double getBalance() {
    return balance;
  }

  public String getAccountNum() {
    return accountNum;
  }
}
