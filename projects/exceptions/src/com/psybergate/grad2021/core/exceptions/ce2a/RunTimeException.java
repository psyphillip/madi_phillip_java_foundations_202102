package com.psybergate.grad2021.core.exceptions.ce2a;

public class RunTimeException extends RuntimeException{

  public RunTimeException(){
    System.out.println("Run time exception reached");
  }

  public RunTimeException(String message) {
    super(message);
  }
}
