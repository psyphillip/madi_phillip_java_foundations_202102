package com.psybergate.grad2021.core.exceptions.hw4b;

public class Controller {

  public static void doAddAccount(String accountNum, String accountHolderName, double balance) {

    try {
      if (AccountValidator.accountExist(accountNum)) {
        System.out
            .println("The account with the similar accountNum already exists, Please pick a different accountNum");
      }
    } catch (AccountDoesNotExistException e) {
      Service.doAddAccount(accountNum, accountHolderName, balance);
    }
  }

  public static void doDeleteAccount(String accountNum) {
    try {
      if (AccountValidator.accountExist(accountNum)) {
        Service.doDeleteAccount(accountNum);
      }
    } catch (AccountDoesNotExistException e) {
      System.out.println("Cannot delete a non-existing account");
    }
  }

  public static void doDeposit(String accountNum, double amount) {
    try {
      if (AccountValidator.accountExist(accountNum)) {
        Service.doDeposit(accountNum, amount);
      }
    } catch (AccountDoesNotExistException e) {
      System.out.println("Cannot do deposit on a non existing account");
      e.printStackTrace();
    }
  }

  public static void doWithdrawal(String accountNum, double amount) {
    try {
      if (AccountValidator.accountExist(accountNum) && AccountValidator.isValidWithdrawal(accountNum, amount)) {
        Service.doWithdrawal(accountNum, amount);
      }
    } catch (InvalidWithdrawalException e) {
      System.out.println("You are tying to withdraw more than an available balance");
    } catch (AccountDoesNotExistException e) {
      System.out.println("Cannot do withdrawal on a non existing account");
    }
  }

  public static void printAccountDetails(String accountNum) {
    try {
      if (AccountValidator.accountExist(accountNum)) {
        Service.printAccountDetails(accountNum);
      }
    } catch (AccountDoesNotExistException e) {
      System.out.println(("Cannot print account details on a non existing account"));
    }
  }
}
