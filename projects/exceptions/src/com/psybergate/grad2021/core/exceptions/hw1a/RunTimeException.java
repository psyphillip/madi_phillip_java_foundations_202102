package com.psybergate.grad2021.core.exceptions.hw1a;

public class RunTimeException {

  public static void main(String[] args) {
    firstMethod();
  }

  public static void firstMethod(){
    secondMethod();
  }

  public static void secondMethod(){
    thirdMethod();
  }

  public static void thirdMethod(){
    forthMethod();
  }

  public static void forthMethod(){
    firthMethod();
  }

  public static void firthMethod(){
    throw new RuntimeException();
  }
}
