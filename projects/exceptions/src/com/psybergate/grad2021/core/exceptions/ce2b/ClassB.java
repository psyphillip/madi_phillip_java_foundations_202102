package com.psybergate.grad2021.core.exceptions.ce2b;

import com.psybergate.grad2021.core.exceptions.ce2a.CheckedException;
import com.psybergate.grad2021.core.exceptions.ce2a.ExceptionExtendingError;
import com.psybergate.grad2021.core.exceptions.ce2a.ExceptionExtendingThrowable;
import com.psybergate.grad2021.core.exceptions.ce2a.RunTimeException;

/**
 * The compiler gives an error because the exceptions are propagated to non-existing methods and as a result they are not handled
 */

public class ClassB {

  public void firstMethod() throws RunTimeException {
    throw new RunTimeException();
  }

  public void secondMethod() throws CheckedException {
    throw new CheckedException();
  }

  public void thirdMethod() throws ExceptionExtendingThrowable {
    throw new ExceptionExtendingThrowable();
  }

  public void forthMethod() throws ExceptionExtendingError {
    throw new ExceptionExtendingError();
  }
}
