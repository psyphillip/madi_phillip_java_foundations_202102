package com.psybergate.grad2021.core.exceptions.hw1a;

/**
 * Observation:
 *
 * If a checked exception is propagated, the calling method will be forced by the compiler to handle the exception
 * If
 */

public class CheckedException {
  public static void main(String[] args) {
    firstMethod();
  }

  public static void firstMethod(){
    try {
      secondMethod();
    } catch (Exception exception) {
      exception.printStackTrace();
//      killException
    }
    System.out.println("Squashed");
  }

  public static void secondMethod() throws Exception {
    thirdMethod();
  }

  public static void thirdMethod() throws Exception {
    forthMethod();
  }

  public static void forthMethod() throws Exception {
    firthMethod();
  }

  public static void firthMethod() throws Exception{
    throw new Exception();
  }
}
