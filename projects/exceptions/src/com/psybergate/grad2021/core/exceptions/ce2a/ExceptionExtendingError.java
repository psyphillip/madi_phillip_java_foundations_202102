package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExceptionExtendingError extends Error{

  public ExceptionExtendingError(){
    System.out.println("Reached an exception extending an error");
  }

  public ExceptionExtendingError(String message) {
    super(message);
  }
}
