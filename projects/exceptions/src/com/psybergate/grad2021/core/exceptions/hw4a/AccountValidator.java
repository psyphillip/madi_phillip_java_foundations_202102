package com.psybergate.grad2021.core.exceptions.hw4a;

public class AccountValidator {

  public static boolean accountExist(String accountNum) throws AccountDoesNotExistException {
    for (Account account : AccountDB.getAccounts()) {
      if (account.getAccountNum().equals(accountNum)) {
        return true;
      }
    }
    throw new AccountDoesNotExistException();
  }

  public static boolean isValidWithdrawal(String accountNum, double amount) throws InvalidWithdrawalException {
    if (AccountDB.getAccount(accountNum).getBalance() > amount) {
      return true;
    }
    throw new InvalidWithdrawalException();
  }
}
