package com.psybergate.grad2021.core.exceptions.hw4a;

public class ClientRunner {
  public static void main(String[] args) {
    //Phillip
    Controller.doAddAccount("123", "Phillip", 10_000);
    Controller.printAccountDetails("123");
    Controller.doDeposit("123", 5_000);
    Controller.printAccountDetails("123");
    Controller.doWithdrawal("123", 12_000);
    Controller.printAccountDetails("123");
    Controller.doDeleteAccount("123");
    Controller.doDeposit("123", 3_000);
    Controller.printAccountDetails("123");
  }
}
