package com.psybergate.grad2021.core.exceptions.hw2a;
import java.sql.SQLException;

public class ClassA {

  public static void methodA() throws ApplicationException {
    try {
      methodB();
    } catch (SQLException throwables) {
      throw new ApplicationException(throwables);
    }
  }

  private static void methodB() throws SQLException {
    throw new SQLException();
  }
}
