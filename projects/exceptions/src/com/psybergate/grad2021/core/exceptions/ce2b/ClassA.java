package com.psybergate.grad2021.core.exceptions.ce2b;

import com.psybergate.grad2021.core.exceptions.ce2a.CheckedException;
import com.psybergate.grad2021.core.exceptions.ce2a.ExceptionExtendingThrowable;

/**
 * All errors from classB has been resolved since the exceptions are handled in this class
 */

public class ClassA {
  public static void main(String[] args) {
    ClassB classB = new ClassB();

    callFirstMethodOfCLassB(classB);
    callSecondMethodOfClassB(classB);
    callThirdMethodOfClassB(classB);
    callForthMethodOfClassB(classB);

  }

  private static void callForthMethodOfClassB(ClassB classB) {
    classB.forthMethod();
  }

  private static void callFirstMethodOfCLassB(ClassB classB) {
    classB.firstMethod();
  }

  private static void callSecondMethodOfClassB(ClassB classB) {
    try {
      classB.secondMethod();
    } catch (CheckedException e) {
      e.printStackTrace();
    }
  }

  private static void callThirdMethodOfClassB(ClassB classB) {
    try {
      classB.thirdMethod();
    } catch (ExceptionExtendingThrowable exceptionExtendingThrowable) {
      exceptionExtendingThrowable.printStackTrace();
    }
  }

}
