package com.psybergate.grad2021.core.exceptions.hw4b;

public class AccountValidator {

  public static boolean accountExist(String accountNum) {
    for (Account account : AccountDB.getAccounts()) {
      if (account.getAccountNum().equals(accountNum)) {
        return true;
      }
    }
    throw new AccountDoesNotExistException();
  }

  public static boolean isValidWithdrawal(String accountNum, double amount) {
    if (AccountDB.getAccount(accountNum).getBalance() > amount) {
      return true;
    }
    throw new InvalidWithdrawalException();
  }
}
