package com.psybergate.grad2021.core.exceptions.hw4a;

public class InvalidWithdrawalException extends Exception {

  public InvalidWithdrawalException() {
    System.out.println("Withdrawal amount is more than the available balance");
  }

}
