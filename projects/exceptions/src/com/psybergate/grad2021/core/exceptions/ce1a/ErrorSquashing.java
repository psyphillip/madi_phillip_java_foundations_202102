package com.psybergate.grad2021.core.exceptions.ce1a;

public class ErrorSquashing extends Exception{
  public static void main(String[] args) {
    printHelloWorld();
  }

  public static void printHelloWorld(){
    try {
      System.out.println("Hello");
      throw new Exception();
    }catch (Exception e){
      e.printStackTrace();
    }
    System.out.println("World");
  }
}
