package com.psybergate.grad2021.core.exceptions.ce2a;

public class TypesOfExceptions {

  public static void main(String[] args) {

    throwCheckedException();
    throwRuntimeException();
    throwExceptionExtendingThrowable();

    try {
      throwExceptionExtendingError();
    } catch (ExceptionExtendingThrowable exceptionExtendingThrowable) {
      exceptionExtendingThrowable.printStackTrace();
    }
  }

  private static void throwExceptionExtendingError() throws ExceptionExtendingThrowable {
    throw new ExceptionExtendingThrowable();
  }

  private static void throwExceptionExtendingThrowable() {
    throw new ExceptionExtendingError();
  }

  private static void throwRuntimeException() {
    throw new RunTimeException();
  }

  private static void throwCheckedException() {
    try{
      if(true){
        throw new CheckedException();
      }
    }catch (CheckedException e){
      System.out.println(e.getMessage());
    }
  }
}
