package com.psybergate.grad2021.core.exceptions.hw2a;

public class Client {
  public static void main(String[] args) {

    try {
      ClassA.methodA();
    } catch (ApplicationException e) {
      System.out.println("There has been an application issue or Database issue");
      System.out.println("See stackTrace below for further details:");
      e.printStackTrace();
    }

  }
}
