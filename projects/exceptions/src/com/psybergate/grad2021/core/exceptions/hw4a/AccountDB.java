package com.psybergate.grad2021.core.exceptions.hw4a;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

  private static List<Account> accounts = new ArrayList<Account>();

  public static void addAccount(Account account){
    accounts.add(account);
  }

  public static void deleteAccount(String accountNum){
    accounts.remove(getAccount(accountNum));
  }

  public static Account getAccount(String accountNum){
    for (Account account : accounts) {
      if(account.getAccountNum().equals(accountNum)){
        return account;
      }
    }
    return null;
  }

  public static List<Account> getAccounts() {
    return accounts;
  }
}
