package com.psybergate.grad2021.core.exceptions.hw4b;

public class Service {

  public static void doDeposit(String accountNum, double amount) {
    Account account = AccountDB.getAccount(accountNum);
    account.deposit(amount);
  }

  public static void doWithdrawal(String accountNum, double amount) {
    Account account = AccountDB.getAccount(accountNum);
    account.withdraw(amount);
  }

  public static void doAddAccount(String accountNum, String accountHolderName, double balance) {
    Account account = new Account(accountNum, accountHolderName, balance);
    AccountDB.addAccount(account);
  }

  public static void doDeleteAccount(String accountNum) {
    AccountDB.deleteAccount(accountNum);
  }

  public static void printAccountDetails(String accountNum) {
    Account account = AccountDB.getAccount(accountNum);
    System.out.println("Account Number: " + accountNum);
    System.out.println("Balance: " + account.getBalance());
  }
}
