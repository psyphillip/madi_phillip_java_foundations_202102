package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExceptionExtendingThrowable extends Throwable{

  public ExceptionExtendingThrowable() {
    System.out.println("Reached an Exception extending a throwable");
  }

  public ExceptionExtendingThrowable(String message) {
    super(message);
  }
}
