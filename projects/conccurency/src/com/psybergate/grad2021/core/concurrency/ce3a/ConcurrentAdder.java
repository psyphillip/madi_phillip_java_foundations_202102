package com.psybergate.grad2021.core.concurrency.ce3a;

public class ConcurrentAdder extends Thread {

  private int sum = 0;

  private int startNumber;

  private int endNumber;

  public ConcurrentAdder(int startNumber, int endNumber) {
    this.startNumber = startNumber;
    this.endNumber = endNumber;
  }

  public int getSum() {
    return sum;
  }

  @Override
  public void run() {
//    System.out.println("startNumber = " + startNumber);
//    System.out.println("endNumber = " + endNumber);
    for (int i = startNumber; i <= endNumber; ++i) {
      sum += i;
    }

//    System.out.println("From " + Thread.currentThread().getName() + " the sum = " + sum);

  }
}
