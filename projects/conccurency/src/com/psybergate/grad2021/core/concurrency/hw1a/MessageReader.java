package com.psybergate.grad2021.core.concurrency.hw1a;

public class MessageReader {

  Messages messages = Messages.getMessageInstance();

  MessageReader messageReader;

  private MessageReader(){
  }

  public MessageReader getMessageReaderInstance(){
    if (messageReader == null) {
      return new MessageReader();
    }
    return messageReader;
  }

  public String readMessage(){
    return messages.readMessage();
  }

}
