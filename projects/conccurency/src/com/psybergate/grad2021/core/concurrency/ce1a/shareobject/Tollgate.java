package com.psybergate.grad2021.core.concurrency.ce1a.shareobject;

public class Tollgate {
  private double amountAccumulated = 0;

  public void addPaidAmount(double amount){
    amountAccumulated += amount;
  }

  public double getAmountAccumulated() {
    return amountAccumulated;
  }
}
