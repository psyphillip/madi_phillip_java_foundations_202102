package com.psybergate.grad2021.core.concurrency.hw1a;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Messages {

  private List<String> unreadMessages = new ArrayList<>();

  private List<String> readMessages = new ArrayList<>();

  private static Messages messages = null;

  private ReentrantLock lock = new ReentrantLock(true);

  private Condition notEmpty = lock.newCondition();

  private Messages() {
  }

  public static Messages getMessageInstance(){
    if (messages == null) {
      return new Messages();
    }
    else{
      return messages;
    }
  }

  public List<String> getUnreadMessages() {
    return unreadMessages;
  }

  public void addNewMessage(String message) {
    lock.lock();

    try {
      unreadMessages.add(message);
      notEmpty.signal();
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  public String readMessage(){
    lock.lock();

    try {
      while (unreadMessages.isEmpty()) {
        notEmpty.await();
      }
      String message = unreadMessages.get(0);
      readMessages.add(message);
      unreadMessages.remove(0);
      return message;
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
    return "";
  }
}
