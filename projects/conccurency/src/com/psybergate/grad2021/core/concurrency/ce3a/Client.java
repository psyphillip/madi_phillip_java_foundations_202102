package com.psybergate.grad2021.core.concurrency.ce3a;

public class Client {

  public static void main(String[] args) throws InterruptedException {

    int range = 10_000;
    int numberOfThreads = 4;

    long startTime = System.nanoTime();
    serialSum(range);
    long timeElapsed = System.nanoTime() - startTime;

    System.out.println(" serial timeElapsed = " + timeElapsed);


    startTime = System.nanoTime();
    parallelSum(range, numberOfThreads);
    timeElapsed = System.nanoTime() - startTime;
    System.out.println(" parallel timeElapsed = " + timeElapsed);
  }

  private static void parallelSum(int range, int numberOfThreads) throws InterruptedException {

    int offset = range / numberOfThreads;

    int sum = 0;

    for (int i = 0; i < numberOfThreads; ++i) {
      if (i + 1 == numberOfThreads) {
        ConcurrentAdder concurrentAdder = new ConcurrentAdder((i * offset) + 1, range);
        concurrentAdder.start();
        concurrentAdder.join();
        sum += concurrentAdder.getSum();
        break;
      }
      ConcurrentAdder concurrentAdder = new ConcurrentAdder((i * offset) + 1, (i + 1) * offset);
      concurrentAdder.start();
      concurrentAdder.join();
      sum += concurrentAdder.getSum();
    }

    System.out.println("sum = " + sum);

  }

  private static void serialSum(int range) {

    int sum = 0;

    for (int i = 1; i <= range; ++i) {
      sum += i;
    }

    System.out.println("sum = " + sum);
  }
}
