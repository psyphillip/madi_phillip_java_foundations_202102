package com.psybergate.grad2021.core.concurrency.hw1a;

public class MessageAppClient {

  public static void main(String[] args) {
    Messages messages = Messages.getMessageInstance();
    Thread t1 = new Thread(new MessageAppUsers(messages, true));
    Thread t2 = new Thread(new MessageAppUsers(messages, false));

    t1.start();
    t2.start();
  }
}
