package com.psybergate.grad2021.core.concurrency.ce1a.shareobject;

import java.util.Random;

public class Car extends Thread {
  private final Tollgate tollgate;

  public Car(Tollgate tollgate) {
    this.tollgate = tollgate;
  }

  @Override
  public void run() {
    for (int i = 1; i <= 5; ++i) {
      int amount = 1;//new Random().nextInt(100);
      synchronized (this) {
        tollgate.addPaidAmount(amount);
        System.out.println("Car number " + Thread.currentThread().getName() + " Paid: " + amount);
      }
    }
  }

}
