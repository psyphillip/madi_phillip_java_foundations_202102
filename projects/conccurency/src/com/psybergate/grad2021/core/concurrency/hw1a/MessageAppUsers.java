package com.psybergate.grad2021.core.concurrency.hw1a;

public class MessageAppUsers implements Runnable {

  private Messages messages;

  private boolean turnToSend;

  public MessageAppUsers() {
  }

  public MessageAppUsers(Messages messages, boolean turnToSend) {
    this.messages = messages;
    this.turnToSend = turnToSend;
  }

  @Override
  public void run() {

    int counter = 10;

    while(counter > 0) {
      if (turnToSend) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        sendMessage();
      } else {
        readMessage();
      }
      --counter;
    }
  }

  private void readMessage() {
    String message = messages.readMessage();
    System.out.println(message + " and " + Thread.currentThread().getName() + " read this message");
    turnToSend = true;
  }

  private void sendMessage() {
    messages.addNewMessage(Thread.currentThread().getName() + " wrote this!");
    turnToSend = false;
  }

  public Messages getMessages() {
    return messages;
  }
}
