package com.psybergate.grad2021.core.concurrency.ce1a;

public class Client {
  public static void main(String[] args) {
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          firstLevel();
        } catch (Exception exception) {
          exception.printStackTrace();
        }
      }

      private void firstLevel() throws Exception {
        secondLevel();
      }

      private void secondLevel() throws Exception {
        throw new Exception();
      }
    });

    Thread t2 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          firstLevel();
        } catch (Exception exception) {
          exception.printStackTrace();
        }
      }

      private void firstLevel() throws Exception {
        secondLevel();
      }

      private void secondLevel() throws Exception {
        throw new Exception();
      }
    });

    t1.start();
    t2.start();
  }
}
