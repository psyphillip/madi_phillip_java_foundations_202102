package com.psybergate.grad2021.core.concurrency.ce1a.shareobject;

public class Client {
  public static void main(String[] args) throws InterruptedException {

    Tollgate tollgate = new Tollgate();

//    int totalAmountPaid = 0;

    Car car1 = new Car(tollgate);
    Car car2 = new Car(tollgate);
    Car car3 = new Car(tollgate);
    Car car4 = new Car(tollgate);
    Car car5 = new Car(tollgate);

    car1.start();
    car2.start();
    car3.start();
    car4.start();
    car5.start();

    car1.join();
    car2.join();
    car3.join();
    car4.join();
    car5.join();

    System.out.println("tollgate.getAmountAccumulated() = " + tollgate.getAmountAccumulated());

//    for (int i = 0; i < 5; ++i) {
//      Car car = new Car(tollgate);
//      car.run();
//      totalAmountPaid += tollgate.getAmountAccumulated();
//    }

//    System.out.println("totalAmountPaid = " + totalAmountPaid);
  }
}
