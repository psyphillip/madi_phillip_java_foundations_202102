package com.psybergate.grad2021.core.concurrency.hw1a;

public class MessageWriter {

  private Messages messages = Messages.getMessageInstance();

  private MessageWriter messageWriter;

  private MessageWriter() {
  }

  public MessageWriter getMessageWriterInstance(){
    if (messageWriter == null){
      return new MessageWriter();
    }
    else {
      return messageWriter;
    }
  }

  public void sendMessage(String message){
    messages.addNewMessage(message);
  }
}
