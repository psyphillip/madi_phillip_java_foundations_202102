package com.psybergate.grad2021.core.generics.hw3a;

import java.util.Collection;

public class Hw3a {

  public static int countEvenNumbers(Collection<Integer> collection) {
    int counter = 0;
    for (Integer integer : collection) {
      if (isEven(integer)) {
        ++counter;
      }
    }
    return counter;
  }

  public static int countOddNumbers(Collection<Integer> collection) {
    int counter = 0;
    for (Integer integer : collection) {
      if (!isEven(integer)) {
        ++counter;
      }
    }
    return counter;
  }

  private static boolean isEven(Integer integer) {
    return integer % 2 == 0;
  }

  public static int countPrimeNumbers(Collection<Integer> collection) {
    int counter = 0;
    for (Integer integer : collection) {
      if (isPrime(integer)) {
        ++counter;
      }
    }
    return counter;
  }

  private static boolean isPrime(Integer integer) {

    if(integer < 2){
      return false;
    }

    for (int i = 2; i < integer / 2; ++i) {
      if (integer % i == 0) {
        return false;
      }
    }
    return true;
  }

  public static int stringCounter(Collection<String> collection) {
    int counter = 0;
    for (String s : collection) {
      if (s.charAt(0) == 'c') {
        ++counter;
      }
    }
    return counter;
  }

  public static int numberOfOverdrawnAccounts(Collection<Account> accounts){
    int counter = 0;
    for (Account account : accounts) {
      if(account.getBalance() < 0){
        ++counter;
      }
    }
    return counter;
  }
}
