package com.psybergate.grad2021.core.generics.hw1b;

import java.util.ArrayList;
import java.util.List;

public class Team <T extends Person> {

  public static final int MAX_TEAM_SIZE = 15;

  private List people = new ArrayList();

  public static int getMaxTeamSize(){
    return MAX_TEAM_SIZE;
  }

  public void addPerson(Person person){
    people.add(person);
  }

  public void anyMethod(Person person){
    //do nothing
  }

  public Person getPerson(int position){
    return (Person) people.get(position);
  }

  public int getNumberOfPersons(){
    return people.size();
  }

  @SuppressWarnings("unchecked")
  public <E extends Person> E[] asArray(E[] e){
    for (int i = 0; i < people.size(); i++) {
      e[i] = (E) getPerson(i);
    }
    return e;
  }

  public void someRandom(List names){
    names.add(new Student());
    System.out.println(names.get(1));
  }
}
