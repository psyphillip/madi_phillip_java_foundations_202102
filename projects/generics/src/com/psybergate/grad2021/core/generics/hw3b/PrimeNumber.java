package com.psybergate.grad2021.core.generics.hw3b;

public class PrimeNumber implements GenericCompareInterface<Integer> {

  @Override
  public boolean count(Integer integer) {
    if (integer < 2) {
      return false;
    }
    for (int i = 2; i < integer / 2; ++i) {
      if (integer % i == 0) {
        return false;
      }
    }
    return true;
  }
}
