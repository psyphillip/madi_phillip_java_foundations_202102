package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.Collection;

public class GreaterThan1000Client {
  
  public static void main(String[] args) {
    Collection<Integer> collection = new ArrayList<>();
    
    collection.add(1000);
    collection.add(2000);
    collection.add(100);
    collection.add(10);
    collection.add(10000);

    System.out.println("greaterThan1000(collection) = " + GreaterThan1000.greaterThan1000(collection));
  }
}
