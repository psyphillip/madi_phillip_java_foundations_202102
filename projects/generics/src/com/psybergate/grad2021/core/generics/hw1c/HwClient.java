package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.List;

public class HwClient {

  public static void main(String[] args) {
    List<String> list = new ArrayList<>();

    list.add("Phillip");
    list.add("Karabo");
    list.add("April");
    list.add("Is");
    list.add("Here");

    System.out.println("reverseStringElements(list) = " + reversedStringElementAfterErasure(list));
  }

  public static List<String> reverseStringElements(List<String> list){
    List<String> resultantList = new ArrayList<>();

    for (String string : list) {
      StringBuilder stringBuilder = new StringBuilder(string).reverse();
      resultantList.add(stringBuilder.toString());
    }

    return resultantList;
  }

  @SuppressWarnings("unchecked")
  public static List reversedStringElementAfterErasure(List list){
    List resultantList = new ArrayList();

    for (Object o : list) {
      StringBuilder stringBuilder = new StringBuilder(o.toString()).reverse();
      resultantList.add(stringBuilder);
    }

    return resultantList;
  }
}
