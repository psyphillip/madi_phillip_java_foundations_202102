package com.psybergate.grad2021.core.generics.hw3a;

public class Account {

  private double balance;

  public Account(double balance) {
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }

  @Override
  public String toString() {
    return "Account{" +
        "balance=" + balance +
        '}';
  }
}
