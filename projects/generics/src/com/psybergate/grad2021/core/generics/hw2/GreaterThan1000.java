package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.Collection;

public class GreaterThan1000 {
  public static Collection<Integer> greaterThan1000(Collection<Integer> collection){
    Collection<Integer> resultantCollection = new ArrayList<>();

    for (Integer integer : collection) {
      if(integer > 1000){
        resultantCollection.add(integer);
      }
    }

    return resultantCollection;
  }

}
