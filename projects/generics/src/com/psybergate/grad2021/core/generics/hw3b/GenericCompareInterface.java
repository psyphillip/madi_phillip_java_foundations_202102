package com.psybergate.grad2021.core.generics.hw3b;

import java.util.Collection;

public interface GenericCompareInterface<T> {

  boolean count(T t);
}
