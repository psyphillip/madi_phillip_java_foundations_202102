package com.psybergate.grad2021.core.generics.hw3b;

import com.psybergate.grad2021.core.generics.hw3a.Account;

public class AccountCheck implements GenericCompareInterface<Account> {
  @Override
  public boolean count(Account account) {
    return account.getBalance() < 0;
  }
}
