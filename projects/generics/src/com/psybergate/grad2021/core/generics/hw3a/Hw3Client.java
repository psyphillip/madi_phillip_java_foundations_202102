package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.Collection;

public class Hw3Client {
  public static void main(String[] args) {

    Collection<Integer> numbers = new ArrayList<>();
    Collection<String> strings = new ArrayList<>();
    Collection<Account> accounts = new ArrayList<>();

    numbers.add(10);
    numbers.add(13);
    numbers.add(19);
    numbers.add(58);
    numbers.add(100);
    numbers.add(1);

    strings.add("cashier");
    strings.add("store");
    strings.add("cleaners");
    strings.add("cash");
    strings.add("security");

    accounts.add(new Account(150_000));
    accounts.add(new Account(0));
    accounts.add(new Account(-150_000));
    accounts.add(new Account(-1));

    System.out.println("Hw3a.countNumberOfPrimes(numbers) = " + Hw3a.countPrimeNumbers(numbers));
    System.out.println("Hw3a.countNumberOfPrimes(numbers) = " + Hw3a.countEvenNumbers(numbers));
    System.out.println("Hw3a.countNumberOfPrimes(numbers) = " + Hw3a.countOddNumbers(numbers));
    System.out.println("Hw3a.stringCounter(strings) = " + Hw3a.stringCounter(strings));
    System.out.println("Hw3a.numberOfOverdrawnAccounts(accounts) = " + Hw3a.numberOfOverdrawnAccounts(accounts));
  }
}
