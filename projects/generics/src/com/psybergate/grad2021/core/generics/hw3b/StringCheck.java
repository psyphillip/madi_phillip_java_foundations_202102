package com.psybergate.grad2021.core.generics.hw3b;

public class StringCheck implements GenericCompareInterface<String> {
  @Override
  public boolean count(String s) {
    return s.charAt(0) == 'c';
  }
}
