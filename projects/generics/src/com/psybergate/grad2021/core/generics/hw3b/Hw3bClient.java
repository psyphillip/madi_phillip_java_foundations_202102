package com.psybergate.grad2021.core.generics.hw3b;

import com.psybergate.grad2021.core.generics.hw3a.Account;

import java.util.ArrayList;
import java.util.Collection;

public class Hw3bClient {
  public static void main(String[] args) {

    Collection<Integer> numbers = new ArrayList<>();
    Collection<String> strings = new ArrayList<>();
    Collection<Account> accounts = new ArrayList<>();

    numbers.add(10);
    numbers.add(13);
    numbers.add(19);
    numbers.add(58);
    numbers.add(100);
    numbers.add(1);

    strings.add("cashier");
    strings.add("store");
    strings.add("cleaners");
    strings.add("cash");
    strings.add("security");

    accounts.add(new Account(150_000));
    accounts.add(new Account(0));
    accounts.add(new Account(-150_000));
    accounts.add(new Account(-1));

    System.out.println("Number of even numbers in " + numbers + " : " + calculate(numbers, new EvenNumber()));
    System.out.println("Number of odd numbers in " + numbers + " : " + calculate(numbers, new OddNumber()));
    System.out.println("Number of prime numbers in " + numbers + " : " + calculate(numbers, new PrimeNumber()));
    System.out.println("Number of string beginning with c in " + strings + " : " + calculate(strings, new StringCheck()));
    System.out.println("Number of overdrawn accounts in " + accounts + " : " + calculate(accounts, new AccountCheck()));

  }

  public static <T> int calculate(Collection<T> collection, GenericCompareInterface<T> compare){
    int counter = 0;
    for (T t : collection) {
      if(compare.count(t)){
        ++counter;
      }
    }
    return counter;
  }
}