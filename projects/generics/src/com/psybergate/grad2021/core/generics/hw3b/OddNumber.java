package com.psybergate.grad2021.core.generics.hw3b;

public class OddNumber implements GenericCompareInterface<Integer> {
  @Override
  public boolean count(Integer integer) {
    return ((Integer) integer) % 2 != 0;
  }
}
