package com.psybergate.jeefnds.servlets.hwweb2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(urlPatterns = "/httpinfoservlet")
public class HttpInfoServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    PrintWriter out = resp.getWriter();

    getHttpHeaderNamesAndValues(req, out);
    getHttpProtocol(req, out);
    getHttpMethod(req, out);
    getHttpRequestURI(req, out);
    getPathInfo(req, out);

  }

  private void getPathInfo(HttpServletRequest req, PrintWriter out) {
    out.println("<h1> HTTP PathInfo </h1>");

    out.println(req.getPathInfo());
  }

  private void getHttpRequestURI(HttpServletRequest req, PrintWriter out) {
    out.println("<h1> HTTP Request URI </h1>");
    out.println(req.getRequestURI());
  }

  private void getHttpMethod(HttpServletRequest req, PrintWriter out){
    out.println("<h1> HTTP Method </h1>");
    out.println(req.getMethod());
  }

  private void getHttpProtocol(HttpServletRequest req, PrintWriter out) {
    out.println("<h1> Protocol </h1>");
    out.println(req.getProtocol());
  }

  private void getHttpHeaderNamesAndValues(HttpServletRequest req, PrintWriter out) {
    out.println("<h1> HTTP Header Names and Values </h1>");
    Enumeration<String> headerNames = req.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String paramName = headerNames.nextElement();
      out.print("<tr><td>" + paramName + "</td>\n");
      String paramValue = req.getHeader(paramName);
      out.println("<td> " + paramValue + "</td></tr>\n");
    }
    out.println("</table>\n</body></html>");
  }
}

