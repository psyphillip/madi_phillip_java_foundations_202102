<%@ page language = "java" contentType="text/html; charset=UTF-8" pageEncoding = "UTF-8" %>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="contentType" content="text/html; charset=utf-8">
	<title> JSP HTTP info </title>
</head>
<body>

	<%
	    getHttpHeaderNamesAndValues();
	    getHttpProtocol();
	    getHttpMethod();
	    getHttpRequestURI();
	    getPathInfo();
	%>

	<%!

		  private void getPathInfo() {
		    out.println("<h1> HTTP PathInfo </h1>");

		    out.println(request.getPathInfo());
		  }

		  private void getHttpRequestURI() {
		    out.println("<h1> HTTP Request URI </h1>");
		    out.println(request.getRequestURI());
		  }

		  private void getHttpMethod(){
		    out.println("<h1> HTTP Method </h1>");
		    out.println(request.getMethod());
		  }

		  private void getHttpProtocol() {
		    out.println("<h1> Protocol </h1>");
		    out.println(request.getProtocol());
		  }

		  private void getHttpHeaderNamesAndValues() {
		    out.println("<h1> HTTP Header Names and Values </h1>");
		    Enumeration<String> headerNames = request.getHeaderNames();
		    while (headerNames.hasMoreElements()) {
		      String paramName = headerNames.nextElement();
		      out.print("<tr><td>" + paramName + "</td>\n");
		      String paramValue = request.getHeader(paramName);
		      out.println("<td> " + paramValue + "</td></tr>\n");
		    }
		    out.println("</table>");
		  }

	%>

</body>
</html>