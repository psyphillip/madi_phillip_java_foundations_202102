package com.psybergate.jeefnds.servlets.arch2.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(urlPatterns = {"/helloworld/*", "/getcurrentdate/*"})
public class DispatcherServlet extends HttpServlet {

  private final Map<String, Controller> CONTROLLERS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadControllers();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String pathInfo = req.getPathInfo();
    resp.getWriter().println(pathInfo);
    resp.setContentType("text/html");
    Controller controller = CONTROLLERS.get(pathInfo);
    resp.getWriter().println(controller.toString());
    if (controller != null) {
      resp.getWriter().println("<h1> " + controller.execute() + "</h1>");
    } else {
      resp.getWriter().println("<h3> invalid request :( </h3>");
    }

  }

  private void loadControllers() {
    Properties properties = new Properties();

    try (InputStream inputStream =
             Thread.currentThread().getContextClassLoader().getResourceAsStream("controller.properties")){
      properties.load(inputStream);

      for (String key : properties.stringPropertyNames()) {
        String classPath = properties.getProperty(key);
        Class clazz = Class.forName(classPath);
        Controller controller = (Controller) clazz.newInstance();
        CONTROLLERS.put("/" + key, controller);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
