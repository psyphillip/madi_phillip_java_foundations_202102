package com.psybergate.jeefnds.servlets.arch2.framework;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Test {

  public static void main(String[] args) {

    Properties properties = new Properties();

    try (InputStream inputStream =
             Thread.currentThread().getContextClassLoader().getResourceAsStream("controller.properties")) {
      properties.load(inputStream);

      for (String key : properties.stringPropertyNames()) {
        String classPath = properties.getProperty(key);
        Class clazz = Class.forName(classPath);
        Controller controller = (Controller) clazz.newInstance();
        System.out.println(key + " => " + classPath);
        //CONTROLLERS.put("/" + key, controller);
      }
    } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
      e.printStackTrace();
    }
  }
}
