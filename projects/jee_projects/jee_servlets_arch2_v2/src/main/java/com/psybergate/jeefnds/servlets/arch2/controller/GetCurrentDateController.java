package com.psybergate.jeefnds.servlets.arch2.controller;

import com.psybergate.jeefnds.servlets.arch2.framework.Controller;

import java.time.LocalDate;

public class GetCurrentDateController implements Controller {
  @Override
  public String execute() {
    return LocalDate.now().toString();
  }
}
