package com.psybergate.jeefnds.servlets.arch2.controller;

import com.psybergate.jeefnds.servlets.arch2.framework.Controller;

public class HelloWorldController implements Controller {
  @Override
  public String execute() {
    return "Hello World";
  }
}
