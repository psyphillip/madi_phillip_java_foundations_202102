package com.psybergate.jeefnds.servlets.arch2.framework;

@FunctionalInterface
public interface Controller {
  String execute();
}
