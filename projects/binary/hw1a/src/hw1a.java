import java.io.FileInputStream;
import java.io.IOException;

public class hw1a {

    private static void readAClassFile() {
        try {

            FileInputStream file = new FileInputStream("hw1a.class");
            int info;

            while ((info = file.read())!=-1) {
                System.out.println(Integer.toHexString(info));
            }
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        readAClassFile();
    }
}
