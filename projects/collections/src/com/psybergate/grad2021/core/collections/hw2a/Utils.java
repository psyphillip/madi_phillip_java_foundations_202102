package com.psybergate.grad2021.core.collections.hw2a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {
  public static void main(String[] args) {
    //not taking a double?????
    Map<Employee, Double> salaries = new HashMap<>();
    List<Employee> employees = new ArrayList<>();

    Employee phil = new Director("123", "Phil", "Madi", 100_000);
    Employee karabo = new Director("125", "Karabo", "Madison", 150_000);
    Employee chris = new Director("111", "Chris", "Naidoo", 200_000);
    Employee noName = new Director("12", "NoName", "NoSurname", 300_000);

    salaries.put(phil, phil.getAnnualSalary());
    salaries.put(karabo, karabo.getAnnualSalary());
    salaries.put(chris, chris.getAnnualSalary());
    salaries.put(noName, noName.getAnnualSalary());

    employees.add(phil);
    employees.add(karabo);
    employees.add(chris);
    employees.add(noName);

    for (Employee employee : employees) {
      System.out.println(salaries.get(employee));
    }
  }
}
