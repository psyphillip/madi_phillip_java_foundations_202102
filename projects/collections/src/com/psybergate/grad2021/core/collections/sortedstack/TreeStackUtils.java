package com.psybergate.grad2021.core.collections.sortedstack;

public class TreeStackUtils {
  public static void main(String[] args) {
    
    SortedStack sortedStack = new TreeStack();
    System.out.println("sortedStack.size() = " + sortedStack.size());
//    sortedStack.push("first");
    sortedStack.push("z");
    sortedStack.push("e");
    sortedStack.push("a");
    sortedStack.push("f");
    sortedStack.push("b");
    sortedStack.push("c");

    System.out.println("sortedStack.size() = " + sortedStack.size());
    sortedStack.pop();
    System.out.println("sortedStack.size() = " + sortedStack.size());

    for (Object o : sortedStack) {
      System.out.println("o = " + o);
    }
  }
}
