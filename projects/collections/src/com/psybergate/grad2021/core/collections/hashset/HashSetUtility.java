package com.psybergate.grad2021.core.collections.hashset;
import java.util.Set;

public class HashSetUtility {

  public static void main(String[] args) {

    Set set = new HashSet();

    set.add("hello");
    set.add("it's");
    set.add("me");
    set.add("I");
    set.add("was");
    set.add("wondering");
    set.add("if");
    set.add("after");
    set.add("all");
    set.add("these");
    set.add("years");
    set.add("you'd");
    set.add("like");
    set.add("to");
    set.add("meet");
    set.add("me");

    System.out.println("Done Inserting");

    for (Object o : set) {
      System.out.println("o = " + o);
    }
  }
}
