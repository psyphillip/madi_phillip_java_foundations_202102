package com.psybergate.grad2021.core.collections.stack;

import com.psybergate.grad2021.core.collections.stack.ArrayStack;

public class ArrayStackUtils {

  public static void main(String[] args) {

    Stack stack = new ArrayStack();
    System.out.println(stack.size());
    stack.push("first");
    stack.push("second");
    stack.push("third");
    System.out.println("stack.get(0) = " + stack.get(0));
    stack.pop();
    System.out.println("stack.size() = " + stack.size());

    for (Object o : stack) {
      System.out.println("o = " + o);
    }
  }
}
