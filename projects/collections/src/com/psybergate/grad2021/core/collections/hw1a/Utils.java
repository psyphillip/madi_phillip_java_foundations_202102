package com.psybergate.grad2021.core.collections.hw1a;

import java.util.SortedSet;
import java.util.TreeSet;

public class Utils {
  public static void main(String[] args) {

    System.out.println("Ascending Sorted set on Employee Number");
    ascendingSet();
    System.out.println();
    System.out.println("Descending Sorted set on Employee Number");
    descendingSet();
  }

  private static void descendingSet() {
    SortedSet<Employee> sortedSet = new TreeSet<Employee>( new ComparatorSortingInDescendingOrder());
//    Set set = new HashSet();
    sortedSet.add(new Director("123", "Phil", "Madi"));
    sortedSet.add(new Director("125", "Karabo", "Madison"));
    sortedSet.add(new Director("111", "Chris", "Naidoo"));
    sortedSet.add(new Director("12", "NoName", "NoSurname"));

    for (Employee employee : sortedSet) {
      System.out.println(employee.toString());
    }
  }

  private static void ascendingSet() {
    SortedSet<Employee> sortedSet = new TreeSet<Employee>();
//    Set set = new HashSet();
    sortedSet.add(new Director("123", "Phil", "Madi"));
    sortedSet.add(new Director("125", "Karabo", "Madison"));
    sortedSet.add(new Director("111", "Chris", "Naidoo"));
    sortedSet.add(new Director("12", "NoName", "NoSurname"));

    for (Employee employee : sortedSet) {
      System.out.println(employee.toString());
    }
  }
}
