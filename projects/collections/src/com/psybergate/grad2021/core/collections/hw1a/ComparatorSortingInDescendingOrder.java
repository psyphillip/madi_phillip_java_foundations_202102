package com.psybergate.grad2021.core.collections.hw1a;

import java.util.Comparator;

public class ComparatorSortingInDescendingOrder implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {
    Employee employee1 = (Employee) o1;
    Employee employee2 = (Employee) o2;

    return -1 * (Integer.parseInt(employee1.getEmployeeNum()) - Integer.parseInt(employee2.getEmployeeNum()));
  }
}
