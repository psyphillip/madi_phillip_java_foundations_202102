package com.psybergate.grad2021.core.collections.hw2a;

public class Manager extends Employee {
  public Manager(String employeeNum, String employeeName, String employeeSurname, double annualSalary) {
    super(employeeNum, employeeName, employeeSurname, annualSalary);
  }
}
