package com.psybergate.grad2021.core.collections.sortedstackdemo;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

public class SortedStack extends AbstractSortedStack{

  private Object[] objects = new Object[10];
  private Comparator comparator;

  public SortedStack() {

  }

  public SortedStack(Comparator comparator) {
    this.comparator = comparator;
  }

  @Override
  public boolean push(Object object) {
    if(size() == objects.length){
      objects = Arrays.copyOf(objects, objects.length*2);
    }

    int insertionIndex = getInsertionIndex(object);
    insertAtIndex(insertionIndex, object);
    return true;
  }

  private void insertAtIndex(int insertionIndex, Object object) {

    Object objectPlaceHolder;

    for(int i = insertionIndex; i <= size(); ++i){
      objectPlaceHolder = objects[i];
      objects[i] = object;
      object = objectPlaceHolder;
    }
  }

  private int getInsertionIndex(Object object) {

    for(int i = 0; i < size(); i++){
      if(((Comparable)objects[i]).compareTo(object) > 0){
        return i;
      }
    }
    return size();
  }

  @Override
  public boolean remove() {
    if (!isEmpty()) {
      for(int i = 1; i < size(); ++i){
        objects[i-1] = objects[i];
      }
      objects[size()-1] = null;
    }
    return true;
  }

  @Override
  public boolean isEmpty() {
    return size() == 0;
  }

  @Override
  public Object pop() {
    Object object = objects[0];
    remove();
    return object;
  }

  @Override
  public Object peek() {
    return objects[0];
  }

  @Override
  public int size() {

    int counter = 0;

    while (objects[counter] != null){
      ++counter;
    }

    return counter;
  }

  @Override
  public Iterator iterator() {

    return new MySortedStackIterator(this);
  }

  @Override
  public Object get(int currentIndex) {

    if (currentIndex < size()) {
      return objects[currentIndex];
    }

    else{
      throw new IndexOutOfBoundsException();
    }
  }
}
