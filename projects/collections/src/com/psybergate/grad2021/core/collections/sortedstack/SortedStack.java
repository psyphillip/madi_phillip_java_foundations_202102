package com.psybergate.grad2021.core.collections.sortedstack;

import java.util.Collection;

public interface SortedStack extends Collection {

  void push(Object object);

  Object pop();

  Object get(int position);

}
