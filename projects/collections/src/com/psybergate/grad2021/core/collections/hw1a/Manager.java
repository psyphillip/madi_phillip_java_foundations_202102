package com.psybergate.grad2021.core.collections.hw1a;

public class Manager extends Employee {
  public Manager(String employeeNum, String employeeName, String employeeSurname) {
    super(employeeNum, employeeName, employeeSurname);
  }
}
