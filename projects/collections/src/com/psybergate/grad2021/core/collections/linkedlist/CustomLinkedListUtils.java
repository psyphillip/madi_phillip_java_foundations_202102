package com.psybergate.grad2021.core.collections.linkedlist;

public class CustomLinkedListUtils {
  public static void main(String[] args) {

    CustomLinkedList myLinkedList = new CustomLinkedList();
    System.out.println("myLinkedList.isEmpty() = " + myLinkedList.isEmpty());
    myLinkedList.add("Phillip");
    System.out.println("myLinkedList.size() = " + myLinkedList.size());
    myLinkedList.add("Phillip");
    System.out.println("myLinkedList.size() = " + myLinkedList.size());
    myLinkedList.add("phillip");
    System.out.println("myLinkedList.size() = " + myLinkedList.size());
    myLinkedList.add(0, "Madi");
    System.out.println("myLinkedList.isEmpty() = " + myLinkedList.isEmpty());
    System.out.println("myLinkedList.size() = " + myLinkedList.size());
    System.out.println("myLinkedList.contains(\"Phillip\") = " + myLinkedList.contains("Phillip"));
    System.out.println("myLinkedList.contains(\"a\") = " + myLinkedList.contains("a"));
    myLinkedList.removeAtIndex(3);
    System.out.println("myLinkedList.size() = " + myLinkedList.size());

    for (Object o : myLinkedList) {
      System.out.println("o = " + o);
    }
  }
}
