package com.psybergate.grad2021.core.collections.hw1a;

public class Administrator extends Employee{
  public Administrator(String employeeNum, String employeeName, String employeeSurname) {
    super(employeeNum, employeeName, employeeSurname);
  }
}
