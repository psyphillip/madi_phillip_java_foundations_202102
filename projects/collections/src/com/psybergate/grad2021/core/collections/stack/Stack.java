package com.psybergate.grad2021.core.collections.stack;

import java.util.Collection;

public interface Stack extends Collection {

  void push(Object object);

  Object pop();

  Object get(int position);

}
