package com.psybergate.grad2021.core.collections.hw3a;

import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class Utils {

  public static void main(String[] args) {


    //force pass by reference on primitives
    SortedSet<String> words = new TreeSet<>(new WordsComparator());

    getWords(words);

    System.out.println();
    System.out.println("Words:");

    printWords(words);
  }

  private static void printWords(SortedSet<String> words) {
    for (String name : words) {
      System.out.println(name);
    }
  }

  private static void getWords(SortedSet<String> words) {

    Scanner scanner = new Scanner(System.in);
    int duplicatedWords = 0;

    System.out.println("Please enter a word below, use -1 to terminate");
    String word = scanner.nextLine();

    while (!word.equals("-1")){
      if(!words.add(word)){
        ++duplicatedWords;
      }
      word = scanner.nextLine();
    }

    System.out.println();
    System.out.println("Words inserted: " + (duplicatedWords + words.size()));

    if(duplicatedWords > 0){
      System.out.println("Duplicated words: " + duplicatedWords);
    }
  }
}
