package com.psybergate.grad2021.core.collections.sortedstackdemo;

public class Utility {
  public static void main(String[] args) {

    SortedStackInterface sortedStack = new SortedStack();

    sortedStack.push("abc");
    sortedStack.push("whatever");
    sortedStack.push("you");
    sortedStack.push("want");
    sortedStack.push("else");
    sortedStack.push("zz2");

    for (Object o : sortedStack) {
      System.out.println("o = " + o);
    }
  }
}
