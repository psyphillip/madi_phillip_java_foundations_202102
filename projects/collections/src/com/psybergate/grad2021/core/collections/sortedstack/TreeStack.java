package com.psybergate.grad2021.core.collections.sortedstack;

import java.util.Comparator;
import java.util.Iterator;

public class TreeStack extends AbstractSortedStack{

  Object[] objects = new Object[100];
  private int size = 0;
  Comparator comparator;

  public TreeStack() {
    comparator = new TreeStackComparator();
  }

//  public TreeStack(Comparator comparator) {
//    this.comparator = comparator;
//  }

  public void printStack(){
    for (Iterator iterator = new SortedStackIterator(this); iterator.hasNext(); ) {

    }

  }

  @Override
  public void push(Object object) {
    if(size == 0){
      objects[0] = object;
      ++size;
      return;
    }

    int insertionPosition = getInsertionPosition(object);

    insertAt(insertionPosition, object);
    ++size;
  }

  private void insertAt(int insertionPosition, Object object) {

    Object holderObject = object;

    for(int i = 0; i < size(); ++i){

      if(i == insertionPosition){
        holderObject = objects[i];
        objects[i] = object;
      }

      else if(i > insertionPosition){
        holderObject = swap(holderObject, objects[i], i);
      }

    }
    objects[size()] = holderObject;
  }

  private Object swap(Object holderObject, Object object, int index){
    objects[index] = holderObject;
    return object;
  }

  private int getInsertionPosition(Object object) {

    for(int i = 0; i < size; ++i){
      if(comparator.compare(objects[i], object) > 0){
        return i;
      }
    }
    return size;
  }

  @Override
  public Object pop() {
    Object object = objects[0];

    for(int i = 1; i < size; ++i){
      objects[i-1] = objects[i];
    }
    objects[size-1] = null;
    --size;
    return object;
  }

  @Override
  public Object get(int position) {

    Object object = null;

    if(position < 0 || position >= size) throw new IndexOutOfBoundsException();

    for (int i = 0; i < size; ++i) {
      if(i == position){
        object = objects[i];
      }
    }

    return object;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    if(objects[0].equals(null)) return true;
    return false;
  }

  @Override
  public boolean contains(Object o) {

    for(int i = 0; i < objects.length; ++i){

      if(objects[i].equals(null)){
        return false;
      }

      if(objects[i].equals(o)){
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new SortedStackIterator(this);
  }
}
