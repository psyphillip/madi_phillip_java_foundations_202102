package com.psybergate.grad2021.core.collections.hashset;

import java.util.Iterator;

public class HashSet extends AbstractHashSet{

  private HashNodeList hashNodeList;

  public HashSet() {
    this.hashNodeList = new HashNodeList();
  }

  @Override
  public int size() {
    return hashNodeList.size();
  }

  @Override
  public boolean isEmpty() {
    return hashNodeList.isEmpty();
  }

  @Override
  public boolean contains(Object o) {
    return hashNodeList.contains(o);
  }

  @Override
  public Iterator iterator() {
    return new MyHashSetIterator(hashNodeList);
  }

  @Override
  public boolean add(Object object) {
    if(!contains(object)){
      hashNodeList.add(object);
    }
    return true;
  }

  @Override
  public boolean remove(Object o) {
    return hashNodeList.remove(o);
  }
}
