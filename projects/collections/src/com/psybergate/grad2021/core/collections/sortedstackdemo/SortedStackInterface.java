package com.psybergate.grad2021.core.collections.sortedstackdemo;

import java.util.Collection;

public interface SortedStackInterface extends Collection {

  boolean push(Object object);

  boolean remove();

  Object pop();

  Object peek();

}
