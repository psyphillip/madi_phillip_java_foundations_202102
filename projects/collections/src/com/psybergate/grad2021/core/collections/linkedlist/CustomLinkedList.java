package com.psybergate.grad2021.core.collections.linkedlist;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class CustomLinkedList extends AbstractLinkedList implements Iterable {

  private Node root;

  private class CustomIterator implements Iterator{
    private int position = 0;

    @Override
    public boolean hasNext() {
      if(isEmpty()){
        return false;
      }
      return position < getSize();
    }

    @Override
    public Object next() {
      if(isEmpty()){
        throw new NoSuchElementException();
      }
      return getNode(position++).getValue();
    }
  }

  public CustomLinkedList() {
    this.root = null;
  }

  public int size(){
    return getSize();
  }

  public boolean isEmpty(){
    return root == null;
  }

  public boolean contains(Object object){

    if(isEmpty()){
      return false;
    }

    else if(object.equals(root.getValue())){
      return true;
    }else{
      Node currentNode = root.getNext();
      while(currentNode != null){
        if(object.equals(currentNode.getValue())){
          return true;
        }
        currentNode = currentNode.getNext();
      }
    }

    return false;
  }

  public Object get(int position){
    return getNode(position).getValue();
  }

  public void add(int position, Object value){

    if(isEmpty() || position >= size()){
      throw new IndexOutOfBoundsException();
    }

    Node newKidOnTheBlock = new Node(value, null, null);

    if(root == null){
      root = newKidOnTheBlock;
    }

    else if(position == 0){
      addAtRoot(newKidOnTheBlock);
    }

    else if(position == getSize()){
      addAtTail(newKidOnTheBlock);
    }
    else if (position > 0 && position < getSize()) {
      Node beforePosition = getNode(position-1);

      newKidOnTheBlock.setNext(beforePosition.getNext());
      beforePosition.setNext(newKidOnTheBlock);
      newKidOnTheBlock.getNext().setPrevious(newKidOnTheBlock);
      newKidOnTheBlock.setPrevious(beforePosition);
    }
  }

  public boolean add(Object value){
    if(root == null){
      root = new Node(value, null, null);
      return true;
    }
    addAtTail(new Node(value, null, null));
    return true;
  }

  public Object set(int position, Object value){
    Node node = getNode(position);
    Object oldValue = node.getValue();
    node.setValue(value);
    return oldValue;
  }

  public void remove(){
    removeRoot();
  }

  public void removeAtIndex(int index){

    if(index < 0 && index >= getSize()){
      throw new IndexOutOfBoundsException();
    }
    else if(index == 0){
      removeRoot();
    }
    else if(index == getSize()-1){
      removeTail();
    }
    else{
      Node toBeRemoved = getNode(index);
      toBeRemoved.getPrevious().setNext(toBeRemoved.getNext());
      toBeRemoved.getNext().setPrevious(toBeRemoved.getPrevious());
    }


  }

  private void removeRoot() {
    root = root.getNext();
    root.getPrevious().setNext(null);
    root.setPrevious(null);
  }

  private void removeTail() {
    Node newTail = getNode(getSize()-2);
    newTail.setNext(null);
  }

  private void addAtTail(Node newKidOnTheBlock) {
    Node tail = getNode(getSize()-1);
    tail.setNext(newKidOnTheBlock);
    newKidOnTheBlock.setPrevious(tail);
  }

  private void addAtRoot(Node newKidOnTheBlock) {
    root.setPrevious(newKidOnTheBlock);
    newKidOnTheBlock.setNext(root);
    root = newKidOnTheBlock;
  }

  private Node getNode(int position){
    if(position >= getSize()){
      throw new IndexOutOfBoundsException();
    }
    else if(position == 0) {
      return root;
    }
    else{
      int counter = 1;
      Node currentNode = root.getNext();
      while (counter != position) {
        currentNode = currentNode.getNext();
        ++counter;
      }
      return currentNode;
    }
  }

  private int getSize(){
    int counter = 0;

    if(root == null){
      return counter;
    }else{
      Node currentNode = root;
      ++counter;
      while(currentNode.getNext() != null){
        ++counter;
        currentNode = currentNode.getNext();
      }
    }

    return counter;
  }

  @Override
  public Iterator iterator() {
    return new CustomIterator();
  }
}
