package com.psybergate.grad2021.core.collections.sortedstackdemo;

import java.util.Iterator;

public class MySortedStackIterator implements Iterator {

  int currentIndex = 0;
  SortedStack sortedStack;

  public MySortedStackIterator(SortedStack sortedStack) {
    this.sortedStack = sortedStack;
  }

  @Override
  public boolean hasNext() {
    return currentIndex < sortedStack.size();
  }

  @Override
  public Object next() {
    return sortedStack.get(currentIndex++);
  }
}
