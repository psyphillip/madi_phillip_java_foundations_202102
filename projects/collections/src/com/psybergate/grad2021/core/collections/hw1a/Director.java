package com.psybergate.grad2021.core.collections.hw1a;

public class Director extends Employee {
  public Director(String employeeNum, String employeeName, String employeeSurname) {
    super(employeeNum, employeeName, employeeSurname);
  }
}
