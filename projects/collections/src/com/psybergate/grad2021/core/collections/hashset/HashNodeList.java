package com.psybergate.grad2021.core.collections.hashset;

public class HashNodeList {

  private Node root;

  public HashNodeList() {
    this.root = null;
  }

  public int size(){
    int counter = 0;

    if(root == null){
      return counter;
    }

    Node currentNode = root;
    ++counter;

    while(currentNode.getNext() != null){
      ++counter;
      currentNode = currentNode.getNext();
    }

    return counter;
  }

  public boolean isEmpty(){
    return root == null;
  }

  public boolean contains(Object object){
    if(isEmpty()){
      return false;
    }

    Node currentNode = root;
    while (currentNode != null) {
      if(object.equals(currentNode.getValue())){
        return true;
      }
      currentNode = currentNode.getNext();
    }
    return false;
  }

  public Node getRoot() {
    return root;
  }

  public boolean add(Object value){
    Node newNode = new Node(value, null);
    if(root == null){
      root = newNode;
      return true;
    }
    Node tailNode = getNode(size()-1);
    tailNode.setNext(newNode);
    return true;
  }

  private Node getNode(int position) {
    if(position == 0){
      return root;
    }
    int counter = 0;
    Node currentNode = root;
    while(counter != position){
      currentNode = currentNode.getNext();
      ++counter;
    }
    return currentNode;
  }

  public boolean remove(Object value){

    if(root.getValue().equals(value)){
      root = root.getNext();
      return true;
    }

    Node currentNode = root;
    while (!currentNode.getNext().getValue().equals(value)) {
      currentNode = currentNode.getNext();
    }
    currentNode.setNext(currentNode.getNext().getNext());
    return true;
  }
}
