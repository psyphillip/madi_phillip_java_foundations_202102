package com.psybergate.grad2021.core.collections.sortedstack;

import java.util.Iterator;

public class SortedStackIterator implements Iterator {

  int currentIndex = 0;

  SortedStack sortedStack;

  public SortedStackIterator(SortedStack sortedStack) {
    this.sortedStack = sortedStack;
  }

  @Override
  public boolean hasNext() {
    if(currentIndex < sortedStack.size()){
      return true;
    }
    return false;
  }

  @Override
  public Object next() {
    return sortedStack.get(currentIndex++);
  }
}
