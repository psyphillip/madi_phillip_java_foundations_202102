package com.psybergate.grad2021.core.collections.hashset;

import java.util.Iterator;

public class MyHashSetIterator implements Iterator {

  HashNodeList hashNodeList;

  Node currentNode;

  public MyHashSetIterator(HashNodeList hashNodeList) {
    this.hashNodeList = hashNodeList;
    this.currentNode = hashNodeList.getRoot();
  }

  @Override
  public boolean hasNext() {
    return currentNode != null;
  }

  @Override
  public Object next() {
    Node node = currentNode;
    currentNode = currentNode.getNext();
    return node.getValue();
  }
}
