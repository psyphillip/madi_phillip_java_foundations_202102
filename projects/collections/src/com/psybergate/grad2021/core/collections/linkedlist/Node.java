package com.psybergate.grad2021.core.collections.linkedlist;

public class Node {
  private Object value;
  private Node previous;
  private Node next;

  public Node(Object value, Node previous, Node next) {
    this.value = value;
    this.previous = previous;
    this.next = next;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  public void setPrevious(Node previous) {
    this.previous = previous;
  }

  public void setNext(Node next) {
    this.next = next;
  }

  public Object getValue() {
    return value;
  }

  public Node getPrevious() {
    return previous;
  }

  public Node getNext() {
    return next;
  }
}
