package com.psybergate.grad2021.core.collections.stack;

import java.util.Collection;
import java.util.Iterator;

public class ArrayStack extends AbstractStack {

  private Object[] objects;

  public ArrayStack() {
    this.objects = new Object[100];
  }

  @Override
  public void push(Object object) {

    Object[] newObjects = new Object[objects.length + 1];

    for (int i = 0; i < newObjects.length; i++) {

      if (i == 0) {
        newObjects[i] = object;
      } else {
        newObjects[i] = objects[i - 1];
      }
    }

    objects = newObjects;
  }

  @Override
  public Object pop() {

    Object[] newObjects = new Object[objects.length - 1];

    Object objectPopped = objects[0];

    for (int i = 0; i < newObjects.length; i++) {
      newObjects[i] = objects[i + 1];
    }

    objects = newObjects;

    return objectPopped;
  }

  @Override
  public Object get(int position) {
    return objects[position];
  }

  @Override
  public int size() {
    int size = 0;
    for (Object object : objects) {
      if(object != null){
        ++size;
      }
      else{
        break;
      }
    }
    return size;
  }

  @Override
  public boolean isEmpty() {
    return objects.length > 0;
  }

  @Override
  public boolean contains(Object o) {
//    for (Object object : objects) {
//      if (object.equals(o)) {
//        return true;
//      }
//    }
//    return false;

    for(Iterator iterator = iterator(); iterator.hasNext();){
      Object object = iterator.next();
      if (object.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new CustomIterator();
  }

  private class CustomIterator implements Iterator {

    private int position = 0;

    @Override
    public boolean hasNext() {
      return position < size();
    }

    @Override
    public Object next() {
      return objects[position++];
    }
  }
}

