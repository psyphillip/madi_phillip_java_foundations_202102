package com.psybergate.grad2021.core.collections.hw2a;

public class Director extends Employee {
  public Director(String employeeNum, String employeeName, String employeeSurname, double annualSalary) {
    super(employeeNum, employeeName, employeeSurname, annualSalary);
  }
}
