package com.psybergate.grad2021.core.collections.hw2a;

public abstract class Employee implements Comparable{

  private String employeeNum;

  private String employeeName;

  private String employeeSurname;

  private double annualSalary;

  public Employee(String employeeNum, String employeeName, String employeeSurname, double annualSalary) {
    this.employeeNum = employeeNum;
    this.employeeName = employeeName;
    this.employeeSurname = employeeSurname;
    this.annualSalary = annualSalary;
  }

  @Override
  public String toString() {
    return "Employee{" +
        "employeeNum='" + employeeNum + '\'' +
        ", employeeName='" + employeeName + '\'' +
        ", employeeSurname='" + employeeSurname + '\'' +
        '}';
  }

  public double getAnnualSalary() {
    return annualSalary;
  }

  @Override
  public int compareTo(Object o) {
    Employee employee = (Employee) o;
//    return this.employeeNum.compareTo(employee.employeeNum);
    return Integer.parseInt(this.employeeNum) - Integer.parseInt(employee.employeeNum);
  }

  public String getEmployeeNum() {
    return employeeNum;
  }
}
