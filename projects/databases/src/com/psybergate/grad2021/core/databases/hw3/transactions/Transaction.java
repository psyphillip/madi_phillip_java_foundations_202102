package com.psybergate.grad2021.core.databases.hw3.transactions;

import com.psybergate.grad2021.core.databases.hw3.accounts.Account;

public abstract class Transaction {

  private String transactionNum;
  Account account;

  public Transaction(String transactionNum, Account account) {
    this.transactionNum = transactionNum;
    this.account = account;
  }

  public abstract String getTransactionType();

  public String getTransactionNum() {
    return transactionNum;
  }
}
