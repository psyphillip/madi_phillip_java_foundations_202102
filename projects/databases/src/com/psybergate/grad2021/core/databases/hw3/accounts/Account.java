package com.psybergate.grad2021.core.databases.hw3.accounts;

import com.psybergate.grad2021.core.databases.hw3.Customer.Customer;
import com.psybergate.grad2021.core.databases.hw3.transactions.Transaction;

import java.util.ArrayList;
import java.util.List;

public abstract class Account {

  private String accountNum;

  Customer customer;

  private double accountBalance;

  List<Transaction> transactions = new ArrayList<>();

  public Account(String accountNum, Customer customer, double accountBalance) {
    this.accountNum = accountNum;
    this.customer = customer;
    this.accountBalance = accountBalance;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public  abstract String getAccountType();

  public double getAccountBalance() {
    return accountBalance;
  }

  public void setAccountBalance(double accountBalance) {
    this.accountBalance = accountBalance;
  }

  public abstract void addTransaction(Transaction transaction);
}
