package com.psybergate.grad2021.core.databases.hw3.accounts;

import com.psybergate.grad2021.core.databases.hw3.Customer.Customer;
import com.psybergate.grad2021.core.databases.hw3.transactions.Transaction;

public class CreditAccount extends Account {

  private static final String ACCOUNT_TYPE = "Credit Account";
  private double overdraft;

  public CreditAccount(String accountNum, Customer customer, double accountBalance, double overdraft) {
    super(accountNum, customer, accountBalance);
    this.overdraft = overdraft;
  }

  public String getAccountType() {
    return ACCOUNT_TYPE;
  }

  public static String getCreditAccountType(){
    return ACCOUNT_TYPE;
  }

  @Override
  public void addTransaction(Transaction transaction) {
    transactions.add(transaction);
  }

  public double getOverdraft() {
    return overdraft;
  }
}
