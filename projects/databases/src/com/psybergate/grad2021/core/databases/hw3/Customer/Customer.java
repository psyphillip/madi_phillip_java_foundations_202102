package com.psybergate.grad2021.core.databases.hw3.Customer;

import com.psybergate.grad2021.core.databases.hw3.accounts.Account;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String customerNum;

  private String customerName;

  private String customerSurname;

  private String city;

  List<Account> accounts = new ArrayList<>();

  public Customer(String customerNum, String customerName, String customerSurname, String city) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerSurname = customerSurname;
    this.city = city;
  }

  public String getCity() {
    return city;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getCustomerName() {
    return customerName;
  }

  public String getCustomerSurname() {
    return customerSurname;
  }

  public List<Account> getAccounts() {
    return accounts;
  }
}
