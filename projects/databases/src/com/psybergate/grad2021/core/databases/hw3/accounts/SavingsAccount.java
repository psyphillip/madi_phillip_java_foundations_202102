package com.psybergate.grad2021.core.databases.hw3.accounts;

import com.psybergate.grad2021.core.databases.hw3.Customer.Customer;
import com.psybergate.grad2021.core.databases.hw3.transactions.Transaction;

public class SavingsAccount extends Account {

  private static final String ACCOUNT_TYPE = "Savings Account";
  private double minimumBalance;

  public SavingsAccount(String accountNum, Customer customer, double accountBalance, double minimumBalance) {
    super(accountNum, customer, accountBalance);
    this.minimumBalance = minimumBalance;
  }

  public String getAccountType() {
    return ACCOUNT_TYPE;
  }

  @Override
  public void addTransaction(Transaction transaction) {
    transactions.add(transaction);
  }

  public static String getSavingsAccountType(){
    return ACCOUNT_TYPE;
  }

  public double getMinimumBalance() {
    return minimumBalance;
  }
}
