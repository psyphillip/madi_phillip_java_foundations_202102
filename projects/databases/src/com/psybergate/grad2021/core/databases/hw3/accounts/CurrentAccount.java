package com.psybergate.grad2021.core.databases.hw3.accounts;

import com.psybergate.grad2021.core.databases.hw3.Customer.Customer;
import com.psybergate.grad2021.core.databases.hw3.transactions.Transaction;

public class CurrentAccount extends Account {

  private static final String ACCOUNT_TYPE = "Current Account";

  public CurrentAccount(String accountNum, Customer customer, double accountBalance) {
    super(accountNum, customer, accountBalance);
  }

  public String getAccountType() {
    return ACCOUNT_TYPE;
  }

  public static String getCurrentAccountType(){
    return ACCOUNT_TYPE;
  }

  @Override
  public void addTransaction(Transaction transaction) {
    transactions.add(transaction);
  }
}
