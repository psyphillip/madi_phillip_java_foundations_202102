package com.psybergate.grad2021.core.databases.hw3.client;

import com.psybergate.grad2021.core.databases.hw3.Customer.Customer;
import com.psybergate.grad2021.core.databases.hw3.accounts.Account;
import com.psybergate.grad2021.core.databases.hw3.accounts.CreditAccount;
import com.psybergate.grad2021.core.databases.hw3.accounts.CurrentAccount;
import com.psybergate.grad2021.core.databases.hw3.accounts.SavingsAccount;
import com.psybergate.grad2021.core.databases.hw3.transactions.Deposit;
import com.psybergate.grad2021.core.databases.hw3.transactions.Transaction;
import com.psybergate.grad2021.core.databases.hw3.transactions.Withdraw;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Client {

  public static void main(String[] args) {

    try {
      Files.deleteIfExists(Paths.get("database_homework.txt"));
      FileWriter homework = new FileWriter("databases_homework.txt");
      homework3a(homework);
      homework3b(homework);
      homework3c(homework);
      homework3d(homework);
      homework3e(homework);
      homework3f(homework);
      homework3g(homework);
      homework3h(homework);
      homework3i(homework);
      homework3j(homework);
      homework.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private static void homework3g(FileWriter homework) throws IOException {
    String query = "select customer.customername,\n" +
        "count(account.customernum),\n" +
        "sum(account.accountbalance)\n" +
        "from customer, account\n" +
        "where customer.customernum = account.accountnum\n" +
        "group by customer.customername having count(account.customernum) > 5 and sum(account.accountbalance) > 1000;";
    homework.append(query);
  }

  private static void homework3j(FileWriter homework) throws IOException {
    String massiveQuery = "select customer.customername,\n" +
        "customer.city,\n" +
        "account.accountnum,\n" +
        "account.accountbalance,\n" +
        "count(transactions.accountnum)\n" +
        "from customer, account, transactions\n" +
        "group by customer.customername, customer.city, account.accountnum, account.accountbalance\n" +
        "having customer.city = 'JHB' or count(transactions.accountnum) > 20;";
    homework.append(massiveQuery);
  }

  private static void homework3i(FileWriter homework) throws IOException {
    String getAccountTypesQuery = "select customer.customername,\n" +
        "account.accountnum,\n" +
        "account.accountbalance\n" +
        "from customer, account \n" +
        "where account.accountType = '"+ CreditAccount.getCreditAccountType() +"' or account.accountType = '"+ SavingsAccount.getSavingsAccountType()+"';";
    System.out.println("getAccountTypesQuery = " + getAccountTypesQuery);

    String sameQueryButWithJoins = "select customer.customername,\n" +
        "account.accountnum,\n" +
        "account.accountbalance\n" +
        "from account \n" +
        "inner join customer on account.customernum = customer.customernum\n" +
        "where account.accountType = '"+ CreditAccount.getCreditAccountType() +"' or account.accountType = '"+ SavingsAccount.getSavingsAccountType()+"';";
    System.out.println("sameQueryButWithJoins = " + sameQueryButWithJoins);

    homework.append(getAccountTypesQuery);
    homework.append(sameQueryButWithJoins);
  }

  private static void homework3h(FileWriter homework) throws IOException {
    String minAndMaxQuery = "select customer.customername,\n" +
        "min(account.accountbalance),\n" +
        "max(account.accountbalance)\n" +
        "from customer, account \n" +
        "where account.customernum = customer.customernum\n" +
        "group by customer.customername\n" +
        "having min(account.accountbalance) > 1000 and max(account.accountbalance) > 8000;";
    homework.append(minAndMaxQuery);
  }

  private static void homework3f(FileWriter homework) throws IOException {
    String customerJointAccountQueryJoinedWithCustomerNameStartingWithJ = "select customer.customername,\n" +
        "customer.customername,\n" +
        "customer.customersurname,\n" +
        "customer.city,\n" +
        "account.accountnum,\n" +
        "account.accountbalance,\n" +
        "account.accounttype from account full join customer on account.customernum = customer.customernum\n" +
        "where customer.name like 'J%';";

    homework.append(customerJointAccountQueryJoinedWithCustomerNameStartingWithJ);
  }

  private static void homework3e(FileWriter homework) throws IOException {
    String customerNum = getRandomNum();

    String gettingCustomerBasedOnNumQuery = "select * from account where customernum = '" + customerNum + "';";
    homework.append(gettingCustomerBasedOnNumQuery);

    String customerJointAccountQuery = "select customer.customername,\n" +
        "customer.customername,\n" +
        "customer.customersurname,\n" +
        "customer.city,\n" +
        "account.accountnum,\n" +
        "account.accountbalance,\n" +
        "account.accounttype from account right join customer on account.customernum = customer.customernum\n" +
        "where customer.city = 'Johannesburg';";
    homework.append(customerJointAccountQuery);

    String accountJointTransactionQuery = "select transactionnum, \n" +
        "transaction.transactiontype, \n" +
        "account.accountnum, \n" +
        "account.accountbalance, \n" +
        "account.accoutntype from transaction right join account on account.accountnum = transaction.accountnum\n" +
        "where account.accountbalance > 6000";
    homework.append(accountJointTransactionQuery);
  }

  private static void homework3d(FileWriter homework) throws IOException {
    String query = "select * from account where accounttype = 'SAVINGS';";
    homework.append(query);
  }

  private static void homework3c(FileWriter homework) throws IOException {
    String query = "delete from account where accountbalance = (select min(accountbalance) from account);";
    homework.append(query);
  }

  private static void homework3b(FileWriter homework) {
  }

  private static void homework3a(FileWriter homework) throws IOException {

    List<Customer> customers = new ArrayList<>();
    List<Account> accounts = new ArrayList<>();

    for (int i = 0; i < 20; ++i) {
      customers.add(getRandomCustomer());
    }

    for (Customer customer : customers) {
      //System.out.println("For customer: " + customer.getCustomerNum());

      String insertCustomerQuery = "insert into customer values ('" + customer.getCustomerNum() + "', '" + customer
          .getCustomerName() + "', '" + customer.getCustomerSurname() + "', '" + customer.getCity() + "');";

      //System.out.println("insertCustomerQuery = " + insertCustomerQuery);

      homework.append(insertCustomerQuery + '\n');

      int randomNumberOfAccounts = getRandomNumber();
      //System.out.println("  Num of accounts: " + randomNumberOfAccounts);

      for (int i = 0; i < randomNumberOfAccounts; i++) {

        Account account = getRandomAccount(customer);
        //System.out.println("    Account Type: " + account.getAccountType());
        accounts.add(account);

        String insertAccountQuery = "insert into account values ('" + account.getAccountNum() + "', '" + customer.
            getCustomerNum() + "', " + (int) account.getAccountBalance() + ", '" + account.getAccountType() + "');";

      //  System.out.println("insertAccountQuery = " + insertAccountQuery);
        homework.append(insertAccountQuery+ '\n');
        int transactions = getRandomNumberOfTransactions();

        for (int j = 0; j < transactions; ++j) {

          Transaction transaction = getRandomTransaction(account);
          //System.out.println("      Transaction: " + transaction.getTransactionType());
          account.addTransaction(transaction);

          String insertTransactionQuery = "insert into transactions values ('" + transaction.getTransactionNum() +
              "', '" + account.getAccountNum() + "', '" + transaction.getTransactionType() + "');";
          System.out.println("insertTransactionQuery = " + insertTransactionQuery);
          homework.append(insertTransactionQuery+ '\n');

        }
      }
    }
  }

  private static Transaction getRandomTransaction(Account account) {
    switch (new Random().nextInt(2)) {
      case (0):
        return new Deposit(getRandomNum(), account, getRandomAmount());
      default:
        return new Withdraw(getRandomNum(), account, getRandomAmount());
    }
  }

  private static double getRandomAmount() {
    return new Random().nextInt();
  }

  private static int getRandomNumberOfTransactions() {
    return new Random().nextInt(100);
  }

  private static Account getRandomAccount(Customer customer) {
    Random random = new Random();
    switch (random.nextInt(3)) {
      case (0):
        return getRandomSavingsAccount(customer);
      case (1):
        return getRandomCurrentAccount(customer);
      default:
        return getRandomCreditAccount(customer);
    }
  }

  private static Account getRandomCreditAccount(Customer customer) {
    return new CreditAccount(getRandomNum(), customer, getRandomBalance(), 1000);
  }

  private static Account getRandomCurrentAccount(Customer customer) {
    return new CurrentAccount(getRandomNum(), customer, getRandomBalance());
  }

  private static Account getRandomSavingsAccount(Customer customer) {
    return new SavingsAccount(getRandomNum(), customer, getRandomBalance(), 100);
  }

  private static double getRandomBalance() {
    return new Random().nextInt(1_000_000);
  }

  private static int getRandomNumber() {
    return new Random().nextInt(8);
  }

  private static Customer getRandomCustomer() {
    return new Customer(getRandomNum(), getRandomName(), getRandomSurname(), getRandomCity());
  }

  private static String getRandomCity() {
    String[] cities = {"Johannesburg", "Durban", "Cape Town", "Polokwane", "Kimberly", "Mahikeng"};
    Random random = new Random();
    return cities[random.nextInt(cities.length)];
  }

  private static String getRandomName() {
    String[] names = {"Phillip", "James", "Samuel", "Lionel", "Mike", "Zakaria", "Jackson"};
    Random random = new Random();
    return names[random.nextInt(names.length)];
  }

  private static String getRandomSurname() {
    String[] names = {"Madi", "Klein", "Williams", "Madison", "Smith", "Gomez", "Gibson"};
    Random random = new Random();
    return names[random.nextInt(names.length)];
  }

  private static String getRandomNum() {
    String number = "";

    Random random = new Random();

    for (int i = 0; i < 10; ++i) {
      number += Integer.toString(random.nextInt(9));
    }
    return number;
  }

}
