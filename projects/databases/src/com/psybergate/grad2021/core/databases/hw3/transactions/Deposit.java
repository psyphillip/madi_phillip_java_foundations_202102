package com.psybergate.grad2021.core.databases.hw3.transactions;

import com.psybergate.grad2021.core.databases.hw3.accounts.Account;

public class Deposit extends Transaction{

  private static final String TRANSACTION_TYPE = "Deposit";

  private double amount;

  public Deposit(String transactionNum, Account account, double amount) {
    super(transactionNum, account);
    this.amount = amount;
  }

  public String getTransactionType() {
    return TRANSACTION_TYPE;
  }

  public void deposit(double amount){
    account.setAccountBalance(account.getAccountBalance() + amount);
  }
}
