package com.psybergate.grad2021.core.databases.hw3.transactions;

import com.psybergate.grad2021.core.databases.hw3.accounts.Account;

public class Withdraw extends Transaction{

  private static final String TRANSACTION_TYPE = "Withdraw";

  private double amount;

  public Withdraw(String transactionNum, Account account, double amount) {
    super(transactionNum, account);
    this.amount = amount;
  }

  public String getTransactionType() {
    return TRANSACTION_TYPE;
  }

  public void withdraw(double amount){
    account.setAccountBalance(account.getAccountBalance() - amount);
  }
}
