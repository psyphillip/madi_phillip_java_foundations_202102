package com.psybergate.grad2021.core.reflection.ce1a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class Client {
  public static void main(String[] args) {
    Class studentClass = Student.class;

    System.out.println("studentClass.getSimpleName() = " + studentClass.getSimpleName());
    System.out.println("studentClass.getPackage().getName() = " + studentClass.getPackage().getName());

    System.out.println();
    System.out.println("studentClass Annotations");
    for (Annotation annotation : studentClass.getAnnotations()) {
      System.out.println("annotation.annotationType().getSimpleName() = " + annotation.annotationType().getSimpleName());
    }

    System.out.println();
    System.out.println("studentClass Interfaces");
    for (Class aClass : studentClass.getInterfaces()) {
      System.out.println("aClass.getSimpleName() = " + aClass.getSimpleName());
    }

    System.out.println();
    System.out.println("public Fields");
    for (Field Field : studentClass.getFields()) {
      System.out.println("Field.getName() = " + Field.getName());
    }

    System.out.println();
    System.out.println("studentClass Fields");
    for (Field Field : studentClass.getDeclaredFields()) {
      System.out.println("Field.getName() = " + Field.getName());
    }

    System.out.println();
    System.out.println("studentClass constructor's parameter list");
    for (Constructor constructor : studentClass.getConstructors()) {
      System.out.println("constructor.getGenericParameterTypes().length = " + constructor.getGenericParameterTypes().length);
      for (Type genericParameterType : constructor.getGenericParameterTypes()) {
        System.out.println("genericParameterType.getTypeName() = " + genericParameterType.getTypeName());
      }
    }

    System.out.println();
    System.out.println("studentClass methods name");
    for (Method declaredMethod : studentClass.getDeclaredMethods()) {
      System.out.println("declaredMethod.getName() = " + declaredMethod.getName());
    }
  }
}
