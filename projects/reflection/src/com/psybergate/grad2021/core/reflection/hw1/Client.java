package com.psybergate.grad2021.core.reflection.hw1;

import com.psybergate.grad2021.core.reflection.ce1a.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Client {

  public static void main(String[] args) {
    Class<Student> studentClass = Student.class;

    System.out.println("studentClass.getSuperclass().getSimpleName() = " + studentClass.getSuperclass().getSimpleName());

    System.out.println("Modifiers for the class");
    System.out
        .println("Modifiers = " + Modifier.toString(studentClass.getModifiers()));

    System.out.println();
    System.out.println("Modifiers and return types for all methods");
    for (Method declaredMethod : studentClass.getDeclaredMethods()) {
      System.out.println("declaredMethod.getName() = " + declaredMethod.getName());
      System.out.println("Modifiers = " + Modifier
          .toString(declaredMethod.getModifiers()));
      System.out.println("ReturnType = " + declaredMethod.getReturnType().getSimpleName());
    }

    System.out.println();
    System.out.println("Modifiers for all fields");
    for (Field declaredField : studentClass.getDeclaredFields()) {
      System.out.println("declaredField.getName() = " + declaredField.getName());
      System.out.println("Modifiers = " + Modifier
          .toString(declaredField.getModifiers()));
    }

  }
}
