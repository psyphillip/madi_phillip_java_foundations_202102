package com.psybergate.grad2021.core.reflection.ce1a;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DomainClass {

}
