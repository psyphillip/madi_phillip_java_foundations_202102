package com.psybergate.grad2021.core.reflection.ce2a;

import com.psybergate.grad2021.core.reflection.ce1a.Student;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Client {
  public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
    Class<Student> studentClass = Student.class;
    Student student;
    try {
      student = studentClass.newInstance();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }

    student = (Student) studentClass.getConstructors()[0].newInstance("123", "Phillip",  "Madi");
    student.printFullName();

    System.out.println("studentClass.getMethod(\"printFullName\").invoke() = " + studentClass.getMethod("printFullName")
        .invoke(student));
  }
}
