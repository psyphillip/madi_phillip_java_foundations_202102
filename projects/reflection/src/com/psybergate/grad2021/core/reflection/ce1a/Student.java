package com.psybergate.grad2021.core.reflection.ce1a;

import java.io.Serializable;

@DomainClass
public class Student implements Serializable, MyInterface {

  private final String studentNum;

  private String firstname;

  private String surname;

  private String degreeQualification;

  public int numberOfCourses;

  public Student() {
    studentNum = "123";
  }

  public Student(String studentNum, String firstname, String surname, String degreeQualification, int numberOfCourses) {
    this.studentNum = studentNum;
    this.firstname = firstname;
    this.surname = surname;
    this.degreeQualification = degreeQualification;
    this.numberOfCourses = numberOfCourses;
  }

  public Student(String studentNum, String firstname, String surname) {
    this.studentNum = studentNum;
    this.firstname = firstname;
    this.surname = surname;
  }

  public String getStudentNum() {
    return studentNum;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getDegreeQualification() {
    return degreeQualification;
  }

  public void setDegreeQualification(String degreeQualification) {
    this.degreeQualification = degreeQualification;
  }

  public int getNumberOfCourses() {
    return numberOfCourses;
  }

  public void setNumberOfCourses(int numberOfCourses) {
    this.numberOfCourses = numberOfCourses;
  }

  public void printFullName(){
    System.out.println(getFirstname() + " " + getSurname());
  }


}
