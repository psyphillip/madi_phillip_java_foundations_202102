04-02-2020_1

Know: 
Privitives
Literals

STATICS:

Doesn't need an object instance to envoke/use it.
Belongs to every object (Think of this as a shared variable)

Class:
No class can be static unless it's an inner class.

Method:
No static method can alter an object state.
Can only alter static members of an object.